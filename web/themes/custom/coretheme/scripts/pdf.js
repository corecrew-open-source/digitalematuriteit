/**
 * @file
 */

(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.pdfbehavior = {
        attach: function attach(context, settings) {
            $('a#download-pdf').once().click(function (e) {
                $('body').append('<div class="ajax-progress"></div>');
                e.preventDefault();
                var html = $('#main-form-content'),
                    selected = $('select[name="start_date"] > option:selected'),
                    name = selected.text(),
                    pageTitle;
                if (name === '' || name === undefined || !name) {
                    name = 'zelfevaluatietool-rapport';
                }
                $(html).find('canvas.chartjs-render-monitor').each(function() {
                    var data = this.toDataURL('image/png');
                    $(this).parent().parent().append('<img class="chart" src="' + data + '" />');
                });
                if (settings.user.uid === 0) {
                    pageTitle = $('#block-paginatitel');
                    html = pageTitle[0].innerHTML+html[0].innerHTML;
                } else {
                    html = html[0].innerHTML;
                }

                $.ajax({
                    'url': '/general/makePdf', 
                    'method': 'POST',
                    'data': {
                        'html': html,
                        'name': name
                    }
                }).done(function(data) {
                    var anchor = document.createElement('a');
                    anchor.href = '/sites/default/files/pdf/' + data.name + '.pdf';
                    anchor.target = '_blank';
                    anchor.download = data.name + '.pdf';
                    $('body').append(anchor);
                    // $('#test').click();
                    anchor.click();
                    $('img.chart').remove();
                    $('.ajax-progress').remove();
                });
            });
        }
    }
})(jQuery, Drupal);