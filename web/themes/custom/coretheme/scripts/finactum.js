/**
 * @file
 */

(function ($, Drupal) {
 $('#edit-companynumber').keyup(function (e) {
   if (/\D/g.test(this.value)) {
     // Filter non-digits from input value.
     this.value = this.value.replace(/\D/g, '');
   }
   if ($(this).val().length === 10) {
     var val = $(this).val();
     $.ajax({
       'url': '/general/getCompanyByVTE',
       'method': 'POST',
       'data': {
         'id': val,
       }
     }).done(function(data) {
       if (data !== 'FALSE') {
         $('#edit-company').val(data.name);
         var bool = false;

         $('#edit-vte option:nth-child(n+2)').each(function(key, value) {
           var val = $(this).text().replace(/ /g,'');
           val = val.replace(/VTE/g, '');
           val = val.replace(/</g, '0-');
           val = val.replace(/,/g, '.');
           var splitArr = val.split('-');
           if (data.vte >= parseFloat(splitArr[0]) && data.vte <= parseFloat(splitArr[1])) {
             $(this).attr('selected', 'selected');
             bool = true;
           }
         });
         if (!bool) {
           $('#edit-vte option:last-child').attr('selected', 'selected');
         }

         $('#edit-startyear option:nth-child(n+2)').each(function(key, value) {
           var val = $(this).text().replace(/ /g,'');
           val = val.replace(/tot/g, '0-');
           var splitArr = val.split('-');
           if (data.foundedYear >= parseFloat(splitArr[0]) && data.foundedYear <= parseFloat(splitArr[1])) {
             $(this).attr('selected', 'selected');
             bool = true;
           }
         });
         if (!bool) {
           $('#edit-startyear option:last-child').attr('selected', 'selected');
         }
       }
     });
   }
 });
})(jQuery, Drupal);
