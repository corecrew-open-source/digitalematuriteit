/**
 * @file
 */

(function ($, Drupal) {
    var currentProgress = $('.progress > .current-progress'),
        question = $('.question.current'),
        category = $('.category.current');

    // Disable the previous button if there is no previous question.
    currentProgress.text(question.data('number'));
    if (currentProgress.text() === '1') {
        $('#navigation-previous').addClass('disabled');
    } else {
        $('#navigation-previous').removeClass('disabled');
    }

    // Set the current class to the first category and question if there is none.
    if (question.length === 0 && category.length === 0 && currentProgress.text() === '1') {
       updateSummary();
       currentProgress.text($('.total-progress').text());
    }

    function nvt() {
        if ($('.question.current > .slider').hasClass('non-aplic')) {
            $('#navigation-non-relevant').addClass('non-aplic');
        } else {
            $('#navigation-non-relevant').removeClass('non-aplic');
        }
    }
    nvt();

    $('.question.current > .slider > #slider-value').on('change', function(){
        $(this).parent().removeClass('non-aplic');
        $('#navigation-non-relevant').removeClass('non-aplic');
    });

    $('.tabs-wrapper > .tab-content > .Qnavigation > a').click(function (e) {
        e.preventDefault();
        var clickLink = $(this);
        if (!$(this).hasClass('disabled') && !$(this).hasClass('no-click')) {
            var direction,
                answerId = $('.question.current').data('answer-id'),
                value = $('.question.current > .slider > .rangeslider > .value').attr('data-value'),
                desc = $('.question.current > .extra-description > textarea').val(),
                questionairyId = $('.tabs-wrapper').data('questionairy-id'),
                oldId = $('.category.current').data('category-id');
            if ($(this).attr('id') === 'navigation-next') {
                direction = 'next';
            } else if ($(this).attr('id') === 'navigation-previous') {
                direction = 'prev';
            } else if ($(this).attr('id') === 'navigation-non-relevant') {
                direction = 'nvt+';
                $('.question.current > .slider > #slider-value').val(0).change();
                $('.question.current > .slider').addClass('non-aplic');
                $('.question.current > .slider > #slider-value').addClass('non-aplic');
                $('.question.current > .slider > .rangeslider > .value').text('nvt');
                $('.question.current > .slider > .rangeslider > .value').attr('data-value', 0);
            }

            if ($('.question.current > .slider > #slider-value').hasClass('non-aplic')) {
                if (direction === 'next') {
                    direction = 'nvt+';
                } else if (direction === 'prev') {
                    direction = 'nvt-';
                }
            }

            $.ajax({
                'url': '/questionairy/save',
                'method': 'POST',
                'data': {
                    'answerId': answerId,
                    'value': value,
                    'desc': desc,
                    'questionairyId': questionairyId,
                    'direction': direction
                },
                'beforeSend': function() {
                    console.log(clickLink);
                    clickLink.addClass('no-click');
                }
            }).done(function(data) {
                if (data === 'TRUE') {
                    if (direction === 'nvt+') {
                        direction = 'next';
                    } else if (direction === 'nvt-') {
                        direction = 'prev';
                    }
                    navigate(direction);
                    $('.progress > .current-progress').text($('.question.current').data('number'));
                    if (currentProgress.text() === '1') {
                        $('#navigation-previous').addClass('disabled');
                    } else {
                        $('#navigation-previous').removeClass('disabled');
                    }
                }
                if (data === 'SUMMARY') {
                    updateSummary();
                    $('body').removeClass('cat'+oldId);
                }

                clickLink.removeClass('no-click');
            });
        }
    });

    $('.navigation-end:not(.popup) a').click(function (e) {
        e.preventDefault();
        $('.navigation-end').fadeOut();
        var questionairyId = $('.tabs-wrapper').data('questionairy-id'),
            type = '';
        if ($(this).attr('id') === 'submit-session') {
            type = 'submit';
        } else {
            type = 'back';
        }
        $.ajax({
           'url': '/questionairy/end-session',
           'method': 'POST',
           'data': {
               'questionairyId': questionairyId,
               'type': type
           }
        }).done(function(data) {
            if (data === 'TRUE') {
                if (type === 'submit') {
                    $(location).attr('href', '/questionairy/results');
                } else if (type === 'back') {
                    location.reload();
                }
            }
        });
    });

    $('a.start-tool').click(function(e) {
       e.preventDefault();
        $.ajax({
            'url': '/questionairy/start',
            'method': 'POST'
        }).done(function(data) {
            if (data !== 'FALSE') {
                $('.navigation-end.popup #submit-session').attr('href', '/questionairy/' + data);
                $('.navigation-end.popup').show();
            } else {
                $(location).attr('href', '/questionairy/new')
            }
        });
    });

    //set right color on page load
    var catId = $('.category.current').data('category-id');
    $('body').addClass('cat'+catId);

    function navigate(direction) {
        var question,
            category,
            catId,
            oldId;
        oldId = $('.category.current').data('category-id');
        if (direction === 'next') {
            question = $('.question.current').next();
            category = $('.category.current').next();
        }
        else if(direction === 'prev') {
            question = $('.question.current').prev();
            category = $('.category.current').prev();
        }
        catId = category.data('category-id');
        if (question.length === 0 && category.length === 0) {
            if (direction === 'next') {
                updateSummary();
                $('body').removeClass('cat'+oldId);
            }
        }
        else if(question.length === 0) {
            $('.category').removeClass('current');
            category.addClass('current');
            category = $('.tab-content > .category.current');
            $('body').addClass('cat'+catId);
            $('body').removeClass('cat'+oldId);
            if (direction === 'next') {
                question = category.find('.question').first();
            }
            else if(direction === 'prev') {
                question = category.find('.question').last();
            }
            $('.question').removeClass('current');
            question.addClass('current');
        } else {
            $('.question').removeClass('current');
            question.addClass('current');
        }
        fullscreenQuestionary();
        nvt();
    }

    function updateSummary() {
        var questionairyId = $('.tabs-wrapper').data('questionairy-id'),
            summary = $('.summary');
        $.ajax({
            'url': '/questionairy/summary',
            'method': 'POST',
            'data': {
                'questionairyId': questionairyId
            }
        }).done(function(data) {
            if (data !== 'FALSE') {
                $.each(data, function(key, value) {
                    var row = $('.summary').find('tr#catergory-' + key);
                    row.find('.answered').text(value.filled);
                    row.find('.nvt').text(value.nvt);
                });
                $('.summary').removeClass('hidden');
                $('.tab-content').addClass('hidden');
                $('.Qnavigation').addClass('hidden');
                fullscreenQuestionary()
            }
        });
    }

})(jQuery, Drupal);