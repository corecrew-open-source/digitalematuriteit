/**
 * @file
 */
$=jQuery;

function fullscreenQuestionary(){
  $('.tabs-wrapper .tab-content .slider').removeAttr('style');
  $('.tabs-wrapper .sidebar .progress, .tabs-wrapper .tab-content .Qnavigation, .tabs-wrapper .summary .navigation-end').removeAttr('style');
  var screenHeight = $(window).height();
  var sidebarHeight = $('.tabs-wrapper .sidebar').innerHeight();
  var sidebarMargin = parseInt($('.tabs-wrapper .sidebar .progress').css('margin-top'));
  var sidebarMarginAlt = parseInt($('.tabs-wrapper .sidebar ul li:last-child').css('margin-bottom'));
  if (sidebarMarginAlt > sidebarMargin) {
    sidebarMargin = sidebarMarginAlt;
  }
  var contentHeight = $('.tabs-wrapper .tab-content').innerHeight();
  var contentMargin = parseInt($('.tabs-wrapper .tab-content .slider').css('margin-top'));
  if (!$('.summary').hasClass('hidden')){
    var contentHeight = $('.tabs-wrapper .summary').innerHeight();
    var contentMargin = parseInt($('.tabs-wrapper .summary .navigation-end').css('margin-top'));
  }
  if (screenHeight >= sidebarHeight && screenHeight >= contentHeight) {
    var sidebarDiff = screenHeight - sidebarHeight + sidebarMargin;
    var contentDiff = screenHeight - contentHeight + contentMargin;
    $('.tabs-wrapper .sidebar .progress').css('margin-top', sidebarDiff);
    $('.tabs-wrapper .tab-content .slider, .tabs-wrapper .summary .navigation-end').css('margin-top', contentDiff);
  } else if (sidebarHeight >= contentHeight) {
    var diff = sidebarHeight - contentHeight + contentMargin;
    $('.tabs-wrapper .tab-content .slider, .tabs-wrapper .summary .navigation-end').css('margin-top', diff);
  } else {
    var diff = contentHeight - sidebarHeight + sidebarMargin;
    $('.tabs-wrapper .sidebar .progress').css('margin-top', diff);
  }
}


(function ($, Drupal) {

/**
 * MatchHeight settings
 */
// Under certain conditions where the size of the page is dynamically changing,
// such as during resize or when adding new elements,
// browser bugs cause the page scroll position to change unexpectedly.
$.fn.matchHeight._maintainScroll = true;
// By default, the _update method is throttled to execute at a maximum rate
// of once every 80ms.
$.fn.matchHeight._throttle = 80;
// Execute matchheight.
$.fn.matchHeight._update();

var matchHeightElements = [
  '.equal-heights article',
];

/**
 * Media Queries
 */
var mobileQuery = 'screen and (max-width:699px)';
var nonMobileQuery = 'screen and (min-width:700px)';

/**
 * Example
 */
Drupal.behaviors.example = {
  attach: function (context, settings) {
    console.log('Drupal Behaviour');
  },
  mobile: function() {
    console.log('Mobile');
  },
  mobileUnmatch : function() {
    console.log('Unmatch Mobile');
  },
  nonMobile: function() {
    console.log('Non Mobile');
  }
}

//equalHeights on images loaded
Drupal.behaviors.imagesLoaded = {
  attach: function (context, settings) {
    var loaded = 0;
    var noOfImages = $("img").length;
    $("img").on('load', function(){ 
      loaded++;
      if(noOfImages === loaded) {
        $(matchHeightElements.join(', ')).matchHeight({ byRow: true, property: 'min-height' });
      }
    }).each(function(){
      if (this.complete) {
        $(this).trigger('load');
      }
    });
  },
};

// select boxes
Drupal.behaviors.select = {
  attach: function (context, settings) {
    $('select').each(function(){
      if (!$(this).parent().hasClass('select')) {
        $(this).wrap('<div class="select"></div>');
      }
    });
  },
};


Drupal.behaviors.result = {
  attach: function (context, settings) {
    //median or average
    if ($('.packed-results-form').length > 0) {
      $('.form-header a.average').addClass('is-active');
      $('#top-table table td:nth-child(4), #top-table table th:nth-child(4)').css('display', 'none');
      $('#middle-table table td:nth-child(4), #middle-table table th:nth-child(4)').css('display', 'none');
      $('#main-form-content > p span:nth-child(2)').css('display', 'none');
      $('#top-chart2').css('display', 'none');
      $('.form-header').on('click', 'a', function(e){
        e.preventDefault();
        $('.form-header a').removeClass('is-active');
        $(this).addClass('is-active');
        if ($(this).hasClass('average')) {
          $('#top-table table td:nth-child(4), #top-table table th:nth-child(4)').css('display', 'none');
          $('#middle-table table td:nth-child(4), #middle-table table th:nth-child(4)').css('display', 'none');
          $('#main-form-content > p span:nth-child(2)').css('display', 'none');
          $('#top-chart2').css('display', 'none');
          $('#top-table table td:nth-child(3), #top-table table th:nth-child(3)').css('display', 'table-cell');
          $('#middle-table table td:nth-child(3), #middle-table table th:nth-child(3)').css('display', 'table-cell');
          $('#main-form-content > p span:nth-child(1)').css('display', 'inline-block');
          $('#top-chart').css('display', 'block');
        } else {
          $('#top-table table td:nth-child(4), #top-table table th:nth-child(4)').css('display', 'table-cell');
          $('#middle-table table td:nth-child(4), #middle-table table th:nth-child(4)').css('display', 'table-cell');
          $('#main-form-content > p span:nth-child(2)').css('display', 'inline-block');
          $('#top-chart2').css('display', 'block');
          $('#top-table table td:nth-child(3), #top-table table th:nth-child(3)').css('display', 'none');
          $('#middle-table table td:nth-child(3), #middle-table table th:nth-child(3)').css('display', 'none');
          $('#main-form-content > p span:nth-child(1)').css('display', 'none');
          $('#top-chart').css('display', 'none');
        }
      });
    }

    //results question table dropdown 
    $('#middle-table table').once().on('click', 'tr.category', function(){
      if (!$(this).hasClass('is-active')) {
        $(this).addClass('is-active');
        var next = $(this).next();
        for (var i = 0; i < $('#middle-table table tbody tr').length; i++) {
          if (next.hasClass('question')) {
            next.slideDown();
            next = next.next();
          } else {
            i = $('#middle-table table tbody tr').length;
          }
        }
      } else {
        $(this).removeClass('is-active');
        var next = $(this).next();
        for (var i = 0; i < $('#middle-table table tbody tr').length; i++) {
          if (next.hasClass('question')) {
            next.slideUp();
            next = next.next();
          } else {
            i = $('#middle-table table tbody tr').length;
          }
        }
      }
    });

    //results question table toelichting
    $('table[data-drupal-selector="edit-table"] tr.question').on('click', 'a', function(event){
      event.preventDefault();
      if (!$(this).hasClass('is-active')) {
        $(this).addClass('is-active');
        $(this).next().slideDown();
      } else {
        $(this).removeClass('is-active');
        $(this).next().slideUp();
      }
    });
    //explanation tabs
    $('#explanation-wrapper p + .form-wrapper .title').addClass('is-active');
    $('#explanation-wrapper .form-wrapper').on('click', '.title', function(){
      if (!$(this).hasClass('is-active')) {
        $('#explanation-wrapper .title.is-active').next().slideUp(0);
        $('#explanation-wrapper .title').removeClass('is-active');
        $(this).next().slideDown(0);
        $(this).addClass('is-active');
        $('#explanation-wrapper').removeAttr('style');
        var wrapperHeight = $('#explanation-wrapper').outerHeight();
        var innerHeight = 0;
        $('#explanation-wrapper .js-form-wrapper').each(function(){
          innerHeight += $(this).height();
        });
        var extraHeight = wrapperHeight + $(this).next().height() - innerHeight;
        console.log(wrapperHeight);
        $('#explanation-wrapper').css({'min-height': extraHeight});
      }
    });


  },
};

//stupid grid on the front page
resizeImgFront()
$(window).resize(function(){
  resizeImgFront()
});

function resizeImgFront(){
  //grid front
  $('.front .region--content #block-mainpagecontent .field--name-field-blocks > .field__item:nth-child(1)').each(function(){
    var marginRight = parseInt($(this).css('margin-right'));
    var colWidth = parseInt($(this).find('.cols--md--2 + .cols--md--2').width());
    var imgWidth = marginRight + colWidth;
    $(this).find('.cols--md--2 + .cols--md--2 img').css({
      'width': imgWidth,
      'max-width': 'none'
    });
  });
  //banner height front
  if ($('body').hasClass('front')) {
    var bannerHeight = $('.region--header').height() + 90 + $('.field--name-field-blocks > .field__item:first-child').innerHeight() + 270;
    $('.banner').height(bannerHeight);
  }
}

/**
 * Toggle local tasks
 * show/hide local tasks
 */
Drupal.behaviors.corecrewActionLinksToggle = {
  attach: function (context, settings) {
    $(context).find('.region--header').once('corecrewActionLinksToggle').each(function () {
      var actionLinks = $('.block-local-tasks-block');
      var cog = $('.action-links-toggle');
      actionLinks.addClass('hidden');
      cog.on('click', function() {
          actionLinks.toggleClass('hidden');
      });
    });
  }
};

/**
 * Toggle mobile header
 * show/hide mobile header
 */
Drupal.behaviors.toggleMobileHeader = {
  attach: function (context, settings) {
    $('.region--header').once().on('click', '#mobile-menu-anchor', function(e){
      e.preventDefault();
      if ($(this).hasClass('is-active')) {
        $('.region--header-mobile-inner').stop().slideUp();
      } else {
        $('.region--header-mobile-inner').stop().slideDown();
      }
      $(this).toggleClass('is-active');
    });
  },
};

/**
 * Accordeon
 * paragraph accordeon
 */
Drupal.behaviors.accordeon = {
  attach: function (context, settings) {
    $('.paragraph--type--accordeon').once().on('click', '.field--name-field-title', function(e){
      e.preventDefault();
      if ($(this).hasClass('active')) {
        $(this).removeClass('active').next().slideUp();
      } else {
        $(this).parent().parent().parent().find('.field--name-field-title.active').removeClass('active').next().slideUp();
        $(this).addClass('active').next().slideDown();
      }
    });
  },
};

/**
 * Helpdesk popup
 */
$('.header-block').on('click', '.helpdesk', function(e){
  e.preventDefault();
  $('.header-block-popin-content').fadeIn();
});

/**
 * Toelichting popup
 */
$('.tips').on('click', '> a', function(e){
  e.preventDefault();
  $('body').append('<div class="popup toelichting"><div class="popup-inner"><div class="popup-cell"><div class="popup-content"></div></div></div></div>');
  var toelichting = $(this).prev().clone();
  toelichting.appendTo('.popup.toelichting .popup-content');
  $('.popup.toelichting').fadeIn();
});

/**
 * popup
 */
$('body').on('click', '.popup:not(.required) .popup-cell', function(e){
  $(this).parent().parent().fadeOut(400, function(){
    if ($(this).hasClass('toelichting')){
      $(this).remove();
    }
  });
});
$('.popup:not(.required) ').on('click', '.popup-content', function(e){
  e.stopPropagation();
});

/**
 * Full screen questionary
 * make the questionairy full screen
 */
Drupal.behaviors.fsQuestionary = {
  attach: function (context, settings) {
    fullscreenQuestionary();
  },
};
$(window).resize(function(){
    fullscreenQuestionary();
});


 /**
  * Range slider
  */
$('input[type="range"]').rangeslider({
  // Feature detection the default is `true`.
  // Set this to `false` if you want to use
  // the polyfill also in Browsers which support
  // the native <input type="range"> element.
  polyfill: false,

  // Default CSS classes
  rangeClass: 'rangeslider',
  disabledClass: 'rangeslider--disabled',
  horizontalClass: 'rangeslider--horizontal',
  verticalClass: 'rangeslider--vertical',
  fillClass: 'rangeslider__fill',
  handleClass: 'rangeslider__handle',

  // Callback function
  onInit: function() {
      var value = $(this.$element).val(),
          textValue = $(this.$element).val() + '%';
      if ($(this.$element).hasClass('non-aplic')) {
          value = 0;
          textValue = 'nvt';
          $(this.$range).addClass('non-aplic');
      }
      $(this.$range).append('<span class="value" data-value="' + value + '">' + textValue + '</span>');
  },

  // Callback function
  onSlide: function(position, value) {
      if ($(this.$range).hasClass('non-aplic')) {
          $(this.$range).removeClass('non-aplic');
          $(this.$element).removeClass('non-aplic');
      }

      $(this.$range).find('.value').text(value + '%').attr('data-value', value);
  },

  // Callback function
  onSlideEnd: function(position, value) {
  }
});


/**
 * Initialize Enquire
 */
enquire.register(mobileQuery, {

  match : function() {
    Drupal.behaviors.example.mobile();
  },

  unmatch : function() {
    Drupal.behaviors.example.mobileUnmatch();
  },

  setup : function() {},
  deferSetup : true,

  destroy : function() {}

}).register(nonMobileQuery, {
  match : function() {
    Drupal.behaviors.example.nonMobile();
    $(matchHeightElements.join(', ')).matchHeight({ byRow: true, property: 'min-height' });
  },

  unmatch : function() {
    $(matchHeightElements.join(', ')).matchHeight({ property: 'min-height', remove: true });
  },

  setup : function() {},
  deferSetup : true,

  destroy : function() {}

});

$('#edit-sector > legend > span.fieldset-legend').addClass('js-form-required form-required');

})(jQuery, Drupal);

