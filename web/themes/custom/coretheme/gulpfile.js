/**
 * @file
 * coretheme gulpfile.
 */

// Set environment
// To generate source map files of the css.
var _dev = false;

// Configure variables
var sassStyle = 'compressed',
    autoprefixerBrowsers = ['last 3 versions', 'IE 8', '> 3%'];  // https://github.com/ai/browserslist#queries

if (_dev) {
  sassStyle = 'expanded';
}

// Configure paths
var paths = {
  styles: {
    src:  'sass/**/*.scss',
    dest: 'css/'
  },
  scripts: {
    src:  'scripts/**/*.js',
    dest: 'js/'
  }
};

// Load plugins
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    gulpLoadPlugins = require('gulp-load-plugins');
    rename = require('gulp-rename'),
    $ = gulpLoadPlugins();

// Watch event function
var watchEvent = function(evt, type) {
  gutil.log(gutil.colors.red(type + ' event:'), gutil.colors.cyan(evt.path.replace(new RegExp('/.*(?=/)/'), '')), 'was', gutil.colors.magenta(evt.type));
};

// Sass compile + prefix task
gulp.task('sass', function() {
  gulp.src(paths.styles.src)
    .pipe($.plumber())
    .pipe(_dev ? $.sourcemaps.init() : gutil.noop())
    .pipe($.sass({
      outputStyle: sassStyle
    }))
    // Prefix CSS depending in required browser support.
    .pipe($.autoprefixer({
      browsers: autoprefixerBrowsers,
      cascade: false
    }))
    .pipe(_dev ? $.sourcemaps.write('./') : gutil.noop())
    .pipe(gulp.dest(paths.styles.dest))
});

// Javascript minify task
gulp.task('scripts', function() {
  gulp.src(paths.scripts.src)
    .pipe($.plumber())
    .pipe($.uglify())
    .pipe($.rename(function (path) {
      path.basename += ".min";
    }))
    .pipe(gulp.dest(paths.scripts.dest));
});

// Watch Task
gulp.task('watch', function() {
  gulp.watch(paths.styles.src, ['sass']).on('change', function(e) {
    watchEvent(e, 'Sass');
  });
  gulp.watch(paths.scripts.src, ['scripts']).on('change', function(e) {
    watchEvent(e, 'Scripts');
  });
});
