(function ($, Drupal) {

// fake end date for questionairy export
// we add 1 day to the end date to include the end date in the query

//if date is already set, lower the date by one
var clonedDate = new Date($('.view-questionairy-export- .js-form-item-field-quest-created-value-1 input').val());
clonedDate.setDate(clonedDate.getDate()-1);
var day = ("0" + clonedDate.getDate()).slice(-2);
var month = ("0" + (clonedDate.getMonth() + 1)).slice(-2);
var year = clonedDate.getFullYear();
clonedDate = year + '-' + month + '-' + day;
$('.view-questionairy-export- .js-form-item-field-quest-created-value-1 input').val(clonedDate);

//create fake field
var fakeField = $('.view-questionairy-export- .js-form-item-field-quest-created-value-1').clone();
fakeField.removeAttr('class').addClass('clone form--inline form-item')
fakeField.find('input').removeAttr('data-drupal-selector').removeAttr('id').attr('name', 'clone').attr('id', 'clone');
fakeField.insertBefore('.view-questionairy-export- .form--inline .form-actions');


//hide real field
$('.view-questionairy-export- .js-form-item-field-quest-created-value-1').css('display', 'none');

//listen to change on the fake field
fakeField.find('input').on('change', function(e){
	var fakeDate = new Date(e.target.value);
	fakeDate.setDate(fakeDate.getDate()+1);
	var day = ("0" + fakeDate.getDate()).slice(-2);
	var month = ("0" + (fakeDate.getMonth() + 1)).slice(-2);
	var year = fakeDate.getFullYear();
	fakeDate = year + '-' + month + '-' + day;
	$('.view-questionairy-export- .js-form-item-field-quest-created-value-1 input').val(fakeDate);
});



})(jQuery, Drupal);
