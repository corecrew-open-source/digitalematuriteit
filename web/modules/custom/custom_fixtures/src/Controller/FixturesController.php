<?php

namespace Drupal\custom_fixtures\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class FixturesController.
 */
class FixturesController extends ControllerBase {

  /**
   * Generatefixtures.
   *
   * @return string
   *   Return Hello string.
   */
  public function generateFixtures() {
      $batch = [
          'title' => $this->t('Generating'),
          'operations' => [
              ['generate', ['']],
          ],
          'finished' => 'custom_fixtures_finished_callback',
          'init_message' => t('Smack Batch is starting.'),
          'progress_message' => t('Processed @current out of @total.'),
          'error_message' => t('Smack My Batch has encountered an error.'),
          'file' => drupal_get_path('module', 'custom_fixtures') . '/custom_fixtures.batch.inc',
      ];

      batch_set($batch);
      return batch_process('<front>');
  }

}
