<?php

function generate($options = [], &$context) {
    $number = 500;

    // Initiate multistep processing.
    if (empty($context['sandbox'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = $number;
    }

    // Process the next 100 if there are at least 100 left. Otherwise,
    // we process the remaining number.
    $batch_size = 100;
    $max = $context['sandbox']['progress'] + $batch_size;
    if ($max > $context['sandbox']['max']) {
        $max = $context['sandbox']['max'];
    }

    $selectsArr = [
        'vte' => 'vte',
        'budget' => 'budget',
        'startyear' =>  'start_year'
    ];

    $elements = [];
    foreach ($selectsArr as $element => $voc) {
        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($voc);
        foreach ($terms as $term) {
            $elements[$element][$term->tid] = $term->tid;
        }
    }

    $sectorArr = [];
    $subsectorArr = [];
    $sectors = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('sector');
    foreach ($sectors as $sector) {
        if ($sector->parents[0] === '0') {
            $sectorArr[$sector->tid] = $sector->tid;
        } else {
            $subsectorArr[$sector->tid] = $sector->tid;
        }
    }

    // Start where we left off last time.
    $start = $context['sandbox']['progress'];
    for ($i = $start; $i < $max; $i++) {
        $userValues = [
            'status' => 1,
            'name' => user_password() . '@corecrew.be',
            'mail' => user_password() . '@corecrew.be',
            'field_user_companynumber' => mt_rand(),
            'field_user_company' => 'test company' . mt_rand(),
            'field_user_name' => 'test name' . mt_rand(),
            'field_user_vte' => ['target_id' => $elements['vte'][array_rand($elements['vte'])]],
            'field_user_startyear' => ['target_id' => $elements['startyear'][array_rand($elements['startyear'])]],
            'field_user_budget' => ['target_id' => $elements['budget'][array_rand($elements['budget'])]],
            'field_user_sector' => ['target_id' => $sectorArr[array_rand($sectorArr)]],
            'field_user_subsector' => ['target_id' => $subsectorArr[array_rand($subsectorArr)]],
            'pass' => user_password(),
        ];
        $user = \Drupal\user\Entity\User::create($userValues);
        $user->save();

        for($j= 0; $j <= 5; $j++) {
            createQuestionairy($user->id());
        }

        // Update our progress!
        $context['sandbox']['progress']++;
    }

    // Multistep processing : report progress.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
}

function createQuestionairy($userId) {
    // Get all the question in order form the taxonomy tree.
    $categories = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('category');
    $returnQuestions = [];
    $entityAnswers = [];
    $totalQuestions = 0;

    $i = 1;
    foreach ($categories as $category) {
        $term = \Drupal\taxonomy\Entity\Term::load($category->tid);
        $questions = $term->get('field_category_questions')->getValue();
        $totalQuestions += count($questions);
        $returnQuestions[$category->tid] = [
            'catId' => $category->tid,
            'catName' => $category->name,
            'questions' => [],
        ];

        foreach ($questions as $question) {
            $node = \Drupal\node\Entity\Node::load($question['target_id']);
            $view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');
            $build = $view_builder->view($node, 'default');

            // Create the empty answers

            $answer = \Drupal\general\Entity\Answer::create([
                'uid' => $userId,
                'name' => $node->label() . ' Answer',
                'field_ans_number' => $i,
                'field_ans_question' => $question['target_id'],
                'field_ans_current' => FALSE,
            ]);

            $test = mt_rand(0, 1);
            if ($test === 1) {
                $answer->set('field_ans_non_aplic', TRUE);
                $answer->set('field_ans_answer', 0);
            } else {
                $answer->set('field_ans_non_aplic', FALSE);
                $answer->set('field_ans_answer', mt_rand(0, 100));
            }

            $answer->setOwnerId($userId);
            $answer->save();
            $entityAnswers[$answer->id()] = $answer->id();

            $returnQuestions[$category->tid]['questions'][$question['target_id']] = [
                'render' => render($build),
                'number' => $i,
                'answerId' => $answer->id(),
                'current' => FALSE,
            ];
            $i++;
        }
    }

    // Create questionairy entity
    $currentDateInput = date('Y-m-d\TH:i:s', mt_rand(1533124800, 1596283200));

    $questionairy = \Drupal\general\Entity\Questionairy::create([
        'uid' => $userId,
        'name' => $currentDateInput,
        'field_quest_created' => $currentDateInput,
        'field_quest_total_questions' => $totalQuestions,
        'field_quest_finished' => TRUE,
        'field_quest_answers' => $entityAnswers,
        'field_quest_user' => $userId,
        'field_quest_unique_id' => uniqid(),
    ]);
    $questionairy->setOwnerId($userId);
    $questionairy->save();

    return $questionairy->id();
}

function custom_fixtures_finished_callback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
        $message = t('success');
    }
    else {
        $message = t('Finished with an error.');
    }
    drupal_set_message($message);
    //$_SESSION['disc_migrate_batch_results'] = $results;
}