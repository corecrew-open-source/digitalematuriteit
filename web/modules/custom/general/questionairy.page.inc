<?php

/**
 * @file
 * Contains questionairy.page.inc.
 *
 * Page callback for Questionairy entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Questionairy templates.
 *
 * Default template: questionairy.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_questionairy(array &$variables) {
  // Fetch Questionairy Entity Object.
  $questionairy = $variables['elements']['#questionairy'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
