(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.generateChartTop = {
    attach: function attach(context, settings) {
      //chart top gemiddeld
      var ctxTop = document.getElementById('top-chart').getContext('2d');
      var pointBackgroundColors = [];
      var myChartTop = new Chart(ctxTop, {
        type: 'radar',
        data: {
          labels: ['', '', '', '', ''],
          datasets: [
            {
              label: 'Gemiddelde',
              data: settings.generateChartTop.avg,
              // data: [42, 45, 25, 25, 100],
              backgroundColor: 'rgba(0,0,0,0)',
              borderColor: 'rgba(0,0,0,0)',
              pointRadius: 6,
              pointHoverRadius: 6,
              pointBackgroundColor: 'rgba(255,255,255,1)',
              pointBorderColor: pointBackgroundColors,
              pointBorderWidth: 2
            },
            {
              label: 'Score',
              data: settings.generateChartTop.user,
              // data: [42, 57, 78, 3, 20],
              fill: true,
              backgroundColor: 'rgba(14, 58, 92, 0.15)',
              borderColor: 'rgba(0,0,0,0)',
              pointRadius: 10,
              pointHoverRadius: 10,
              pointBackgroundColor: pointBackgroundColors
            },
          ]
        },
        options: {
          scale: {
            display: true,
            ticks: {
              min: 0,
              max: 100,
              callback: function(value, index, values) {
                  if (index === values.length - 1) return '100%';
                  else if (index === 0) return '0%';
                  else return '';
              }
            }
          },
          legend: {
            display: false
          }
        }
      });

      for (var i = 0; i < myChartTop.data.datasets[0].data.length; i++) {
          if (i == 0) {
              pointBackgroundColors.push("#C7402D");
          } else if (i == 1) {
              pointBackgroundColors.push("#15959F");
          } else if (i == 2) {
              pointBackgroundColors.push("#EC9770");
          } else if (i == 3) {
              pointBackgroundColors.push("#76448D");
          } else if (i == 4) {
              pointBackgroundColors.push("#8A9161");
          } 
      }
      myChartTop.update();

      //chart top mediaan
      var ctxTop2 = document.getElementById('top-chart2').getContext('2d');
      var pointBackgroundColors2 = [];
      var myChartTop2 = new Chart(ctxTop2, {
        type: 'radar',
        data: {
          labels: ['', '', '', '', ''],
          datasets: [
            {
              label: 'Mediaan',
              data: settings.generateChartTop.median,
              // data: [22, 70, 55, 32, 65],
              backgroundColor: 'rgba(0,0,0,0)',
              borderColor: 'rgba(0,0,0,0)',
              pointRadius: 6,
              pointHoverRadius: 6,
              pointBackgroundColor: 'rgba(255,255,255,1)',
              pointBorderColor: pointBackgroundColors,
              pointBorderWidth: 2
            },
            {
              label: 'Score',
              data: settings.generateChartTop.user,
              // data: [42, 57, 78, 3, 20],
              fill: true,
              backgroundColor: 'rgba(14, 58, 92, 0.15)',
              borderColor: 'rgba(0,0,0,0)',
              pointRadius: 10,
              pointHoverRadius: 10,
              pointBackgroundColor: pointBackgroundColors
            },
          ]
        },
        options: {
          scale: {
            display: true,
            ticks: {
              min: 0,
              max: 100,
              callback: function(value, index, values) {
                  if (index === values.length - 1) return '100%';
                  else if (index === 0) return '0%';
                  else return '';
              }
            }
          },
          legend: {
            display: false
          }
        }
      });

      for (var i = 0; i < myChartTop2.data.datasets[0].data.length; i++) {
          if (i == 0) {
              pointBackgroundColors2.push("#C7402D");
          } else if (i == 1) {
              pointBackgroundColors2.push("#15959F");
          } else if (i == 2) {
              pointBackgroundColors2.push("#EC9770");
          } else if (i == 3) {
              pointBackgroundColors2.push("#76448D");
          } else if (i == 4) {
              pointBackgroundColors2.push("#8A9161");
          } 
      }
      myChartTop2.update();

      if (document.getElementById('bottom-chart')) {
        //chart bottom gemiddeld
        var ctxBottom = document.getElementById('bottom-chart').getContext('2d');
        var pointBackgroundColors3 = [];
        var myChartBottom = new Chart(ctxBottom, {
          type: 'radar',
          data: {
            labels: ['', '', '', '', ''],
            datasets: [
              {
                label: 'Average Chart',
                data: settings.generateChartBottom.avg,
                // data: [42, 45, 25, 25, 100],
                backgroundColor: 'rgba(0,0,0,0)',
                borderColor: 'rgba(0,0,0,0)',
                pointRadius: 6,
                pointHoverRadius: 6,
                pointBackgroundColor: 'rgba(255,255,255,1)',
                pointBorderColor: pointBackgroundColors3,
                pointBorderWidth: 2
              },
              {
                label: 'User Chart',
                data: settings.generateChartBottom.user,
                // data: [42, 57, 78, 3, 20],
                fill: true,
                backgroundColor: 'rgba(14, 58, 92, 0.15)',
                borderColor: 'rgba(0,0,0,0)',
                pointRadius: 10,
                pointHoverRadius: 10,
                pointBackgroundColor: pointBackgroundColors3
              },
            ]
          },
          options: {
            scale: {
              display: true,
              ticks: {
                min: 0,
                max: 100,
                callback: function(value, index, values) {
                    if (index === values.length - 1) return '100%';
                    else if (index === 0) return '0%';
                    else return '';
                }
              }
            }, 
            legend: {
              display: false
            }
          }
        });

        for (var i = 0; i < myChartBottom.data.datasets[0].data.length; i++) {
            if (i == 0) {
                pointBackgroundColors3.push("#C7402D");
            } else if (i == 1) {
                pointBackgroundColors3.push("#15959F");
            } else if (i == 2) {
                pointBackgroundColors3.push("#EC9770");
            } else if (i == 3) {
                pointBackgroundColors3.push("#76448D");
            } else if (i == 4) {
                pointBackgroundColors3.push("#8A9161");
            } 
        }
        myChartBottom.update();
      }

      if (settings.privacy !== undefined) {
          $('.edit-privacy input[type="radio"]').removeAttr('checked');
          $('.edit-privacy input[type="radio"][value="' + settings.privacy +'"]').attr('checked', 'checked');
      }
    }
  };
})(jQuery, Drupal);