<?php

namespace Drupal\general;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\elasticsearch_connector\ClusterManager;
use Drupal\elasticsearch_connector\ElasticSearch\ClientManager;
use Drupal\elasticsearch_connector\ElasticSearch\ClientManagerInterface;
use nodespark\DESConnector\Elasticsearch\Aggregations\Bucket\Terms;
use nodespark\DESConnector\Elasticsearch\Aggregations\Metrics\Avg;
use nodespark\DESConnector\Elasticsearch\Aggregations\Metrics\Sum;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ElasticsearchQueryService.
 */
class ElasticsearchQueryService implements ContainerInjectionInterface {

  /**
   * Client manager.
   *
   * @var \Drupal\elasticsearch_connector\ElasticSearch\ClientManager
   */
  protected $clientManager;

  /**
   * The cluster manager.
   *
   * @var \Drupal\elasticsearch_connector\ClusterManager
   */
  protected $clusterManager;

  /**
   * Constructor for the
   *
   * @param \Drupal\elasticsearch_connector\ElasticSearch\ClientManagerInterface $clientManager
   *   The client manager.
   * @param \Drupal\elasticsearch_connector\ClusterManager $clusterManager
   */
  public function __construct(ClientManagerInterface $clientManager, ClusterManager $clusterManager) {
    $this->clientManager = $clientManager;
    $this->clusterManager = $clusterManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('elasticsearch_connector.client_manager'),
      $container->get('elasticsearch_connector.cluster_manager')
    );
  }

  /**
   * Query elasticsearch.
   *
   * @param array $vars
   *   An array of parameters.
   *
   * @return \nodespark\DESConnector\Elasticsearch\Response\SearchResponseInterface
   *   The response from elasticsearch.
   */
  public function query(array $vars) {
    $clusters = $this->clusterManager->loadAllClusters();
    $client = $this->clientManager->getClientForCluster($clusters['local']);
    $params['body'] = [];

    if (!empty($vars['filters'])) {
      $filters = [];
      foreach ($vars['filters'] as $name => $value) {
        $filters[] = [
          'match' => [$name => $value],
        ];
      }

      $params['body']['query']['bool']['filter'] = $filters;
    }

    if (!empty($vars['must'])) {
      $params['body']['query']['bool']['must'] = $vars['must'];
    }

    if (!empty($vars['source'])) {
      $params['body']['_source'] = $vars['source'];
    }

    if (!empty($vars['aggs'])) {
      foreach ($vars['aggs'] as $name => $value) {
        switch ($name) {
          case 'sum':
            $aggregation = new Sum(key($value), reset($value), $name);
            break;

          case 'avg':
            $aggregation = new Avg(key($value), reset($value));
            break;

          case 'terms':
            $aggregation = new Terms(key($value), reset($value));
            break;
        }
        $client->aggregations()->setAggregation($aggregation);
      }
    }

    $client->aggregations()->applyAggregationsToParams($params);
    // Set max size. In case in the future there are more possible records,
    // we will need to use paging.
    $params['body']['size'] = 10000;
    return $client->search($params);
  }


}
