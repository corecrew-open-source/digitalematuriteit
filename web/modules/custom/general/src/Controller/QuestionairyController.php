<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\general\Entity\Answer;
use Drupal\general\Entity\Questionairy;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class QuestionairyController.
 */
class QuestionairyController extends ControllerBase {
    public function startQuestionairy($id) {
        if ($id === 'new') {
            $data = $this->createQuestionairy();
            return $this->redirect('general.questionairy_controller_StartQuestionairy', ['id' => $data]);
        } else {
            $query = \Drupal::entityQuery('questionairy')->condition('field_quest_unique_id', $id);
            $questionairyId = $query->execute();
            if (count($questionairyId) > 0) {
                $questionairy = Questionairy::load(reset($questionairyId));
                if (isset($questionairy->get('field_quest_user')->getValue()[0])) {
                    $userQuest = $questionairy->get('field_quest_user')->getValue()[0]['target_id'];
                    $currentUserId = \Drupal::currentUser()->id();
                    if ((int) $userQuest === (int) $currentUserId || (int) $userQuest === 0) {
                        $categories = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('category');
                        $returnQuestions = [];
                        $entityAnswers = [];

                        $answerIds = $questionairy->get('field_quest_answers')->getValue();
                        foreach ($answerIds as $answerId) {
                            $answerEntity = Answer::load($answerId['target_id']);
                            $questionId = $answerEntity->get('field_ans_question')->getValue()[0]['target_id'];
                            $answer =  [
                                'id' => $answerId['target_id'],
                                'current' => $answerEntity->get('field_ans_current')->getValue()[0]['value'],
                                'value' => 50,
                                'desc' => '',
                                'nvt' => $answerEntity->get('field_ans_non_aplic')->getValue()[0]['value'],
                            ];

                            if (isset($answerEntity->get('field_ans_answer')->getValue()[0])) {
                                $answer['value'] = $answerEntity->get('field_ans_answer')->getValue()[0]['value'];
                            }
                            if (isset($answerEntity->get('field_ans_desciption')->getValue()[0])) {
                                $answer['desc'] = $answerEntity->get('field_ans_desciption')->getValue()[0]['value'];
                            }

                            $entityAnswers[$questionId] = $answer;
                        }

                        $i = 1;
                        foreach ($categories as $category) {
                            $term = Term::load($category->tid);
                            $questions = $term->get('field_category_questions')->getValue();
                            $returnQuestions[$category->tid] = [
                                'catId' => $category->tid,
                                'catName' => $category->name,
                                'questions' => [],
                                'current' => '0',
                            ];

                            foreach ($questions as $question) {
                                $node = Node::load($question['target_id']);
                                $view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');
                                $build = $view_builder->view($node, 'default');
                                $build['#answer'] = [
                                    'value' => $entityAnswers[$question['target_id']]['value'],
                                    'desc' => $entityAnswers[$question['target_id']]['desc'],
                                    'nvt' => $entityAnswers[$question['target_id']]['nvt'],
                                ];
                                $render = \Drupal::service('renderer')->renderPlain($build);

                                $current = $entityAnswers[$question['target_id']]['current'];
                                $returnQuestions[$category->tid]['questions'][$question['target_id']] = [
                                    'render' => $render,
                                    'number' => $i,
                                    'answerId' => $entityAnswers[$question['target_id']]['id'],
                                    'current' => $current,
                                ];

                                if ($current === '1') {
                                    $returnQuestions[$category->tid]['current'] = '1';
                                }

                                $i++;
                            }
                        }

                        return [
                            '#theme' => 'start_test',
                            '#questionairy' => $questionairy,
                            '#questions' => $returnQuestions,
                            '#unique_id' => $id,
                        ];
                    } else {
                        return new Response('', Response::HTTP_FORBIDDEN);
                    }
                }
            } else {
                throw new NotFoundHttpException();
            }
        }
    }

    /**
     * Save the answer (only via ajax available).
     */
    public function saveAnswer() {
        $isAjax = \Drupal::request()->isXmlHttpRequest();
        if ($isAjax) {
            $request = \Drupal::request();
            $answerId = $request->get('answerId');
            $value = $request->get('value');
            $desc = $request->get('desc');
            $direction = $request->get('direction');

            // Set all answers current on false.
            $questionairy = Questionairy::load($request->get('questionairyId'));
            $answerIds = $questionairy->get('field_quest_answers')->getValue();
            $current = 0;
            $i = 0;
            foreach ($answerIds as $answerEntityId) {
                $answerEntity = Answer::load($answerEntityId['target_id']);
                $answerEntity->set('field_ans_current', 0);
                $answerEntity->save();

                if($answerEntityId['target_id'] == $answerId) {
                    if ($direction === 'next' || $direction === 'nvt+') {
                        $current = $i + 1;
                    } else if ($direction === 'prev' || $direction === 'nvt-') {
                        $current = $i - 1;
                    }
                }
                $i++;
            }

            // Save the answer
            $answer= Answer::load($answerId);
            $answer->set('field_ans_desciption', $desc);

            if ($direction === 'nvt+' || $direction === 'nvt-') {
                $answer->set('field_ans_non_aplic', 1);
                $answer->set('field_ans_answer', 0);
            } else {
                $answer->set('field_ans_non_aplic', 0);
                $answer->set('field_ans_answer', $value);
            }
            $answer->save();

            if ($current < $i) {
                $currentAnswerEntity = Answer::load($answerIds[$current]['target_id']);
                $currentAnswerEntity->set('field_ans_current', 1);
                $currentAnswerEntity->save();
                return new Response('TRUE');
            } else {
                return new Response('SUMMARY');
            }
        }
        return new Response('FALSE');
    }

    function calcuteWeightedValue($questionairyId)
    {
        $categoryQuestionArr = [];
        $categories = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('category');
        foreach ($categories as $category) {
            $term = Term::load($category->tid);
            $questions = $term->get('field_category_questions')->referencedEntities();
            foreach ($questions as $question) {
                $categoryQuestionArr[$question->id()] = [
                    'cat' => $category->tid,
                    'weight' => $question->get('field_question_calculate_weight')->getValue()[0]['value'],
                ];
            }
        }

        $categorieSums = [];
        $questionairy = Questionairy::load($questionairyId);
        foreach ($questionairy->get('field_quest_answers')->referencedEntities() as $answer) {
            // Calculate sum of categories if not non-aplic
            $question = $answer->get('field_ans_question')->referencedEntities()[0];
            if (!isset($categorieSums[$categoryQuestionArr[$question->id()]['cat']])) {
                $categorieSums[$categoryQuestionArr[$question->id()]['cat']] = 0;
            }
            if ($answer->get('field_ans_non_aplic')->getValue()[0]['value'] == 0) {
                $weight = $question->get('field_question_calculate_weight')->getValue()[0]['value'];
                $categorieSums[$categoryQuestionArr[$question->id()]['cat']] += (float)$weight;
            }
        }

        foreach ($questionairy->get('field_quest_answers')->referencedEntities() as $answer) {
            $value = $answer->get('field_ans_answer')->getValue()[0]['value'];
            $questionId = $answer->get('field_ans_question')->getValue()[0]['target_id'];
            $calculatedValue = (($value * ($categoryQuestionArr[$questionId]['weight'] / 100)) / $categorieSums[$categoryQuestionArr[$questionId]['cat']]) * 100;
            $answer->set('field_ans_answer_calc', $calculatedValue);
            $answer->save();
        }
    }

    /**
     * Update the summary (only via ajax available).
     */
    public function updateSummary(){
        $isAjax = \Drupal::request()->isXmlHttpRequest();
        if ($isAjax) {
            $request = \Drupal::request();
            $questionairy = Questionairy::load($request->get('questionairyId'));
            $answerIds = $questionairy->get('field_quest_answers')->getValue();

            $categoryQuestionArr = [];
            $categories = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('category');
            foreach ($categories as $category) {
                $term = Term::load($category->tid);
                $questions = $term->get('field_category_questions')->getValue();
                foreach ($questions as $question) {
                    $categoryQuestionArr[$question['target_id']] = $category->tid;
                 }
            }

            $returnArr = [];
            foreach ($answerIds as $answerEntityId) {
                $answerEntity = Answer::load($answerEntityId['target_id']);
                $questionId = $answerEntity->get('field_ans_question')->getValue()[0]['target_id'];
                $categoryId = $categoryQuestionArr[$questionId];

                if (!isset($returnArr[$categoryId]['nvt'])) {
                    $returnArr[$categoryId]['nvt'] = 0;
                }
                if (!isset($returnArr[$categoryId]['filled'])) {
                    $returnArr[$categoryId]['filled'] = 0;
                }
                if ($answerEntity->get('field_ans_non_aplic')->getValue()[0]['value'] === '1') {
                    $returnArr[$categoryId]['nvt']++;
                } else {
                    $returnArr[$categoryId]['filled']++;
                }
            }

            return new JsonResponse($returnArr);
        }

        return new Response('FALSE');
    }

    /**
     * buttons on the last screen of the session calls (only available thrue ajax)
     */
    public function endSession() {
        $isAjax = \Drupal::request()->isXmlHttpRequest();
        if ($isAjax) {
            $request = \Drupal::request();
            $type = $request->get('type');
            $questionairy = Questionairy::load($request->get('questionairyId'));
            if ($type === 'submit') {
                $this->calcuteWeightedValue($request->get('questionairyId'));
                $questionairy->set('field_quest_finished', 1);
                $questionairy->save();
            } else if ($type === 'back') {
                $answerIds = $questionairy->get('field_quest_answers')->getValue();
                foreach ($answerIds as $answerEntityId) {
                    $answerEntity = Answer::load($answerEntityId['target_id']);
                    $answerEntity->set('field_ans_current', 0);
                    $answerEntity->save();
                }
                $firstAnswer = Answer::load($answerIds[0]['target_id']);
                $firstAnswer->set('field_ans_current', 1);
                $firstAnswer->save();
            }

            return new Response('TRUE');
        }
        return new Response('FALSE');
    }


    /**
     * checks to create new session or show popup for old (only available thrue ajax)
     */
    public function checkNewOrOldQuestionairy() {
        $isAjax = \Drupal::request()->isXmlHttpRequest();
        if ($isAjax) {
            $currentUserId = \Drupal::currentUser()->id();
            if (isset($_COOKIE['last_questionairy'])) {
                $query = \Drupal::entityQuery('questionairy')->condition('field_quest_finished', 0)->condition('field_quest_unique_id', $_COOKIE['last_questionairy']);
                $questionairyId = $query->execute();
                if (count($questionairyId) > 0) {
                    $questionairy = Questionairy::load(reset($questionairyId));
                    if (isset($questionairy->get('field_quest_user')->getValue()[0])) {
                        $userQuest = $questionairy->get('field_quest_user')->getValue()[0]['target_id'];
                        if ((int) $userQuest === (int) $currentUserId) {
                            return new Response($_COOKIE['last_questionairy']);
                        } else {
                            unset($_COOKIE['last_questionairy']);
                            setcookie('last_questionairy', '', time() - 3600, '/');
                            return new Response('FALSE');
                        }
                    }
                }
                return new Response('FALSE');
            } else {
                // if the current user is not 'anonymous' get the last unfinished questionairy en set the id in the cookie.
                if ((int) $currentUserId === 0) {
                    return new Response('FALSE');
                }

                $query = \Drupal::entityQuery('questionairy')->condition('field_quest_user', $currentUserId)->condition('field_quest_finished', 0)->sort('field_quest_created', 'DESC');
                $questionairyId = $query->execute();
                if (count($questionairyId) === 0) {
                    return new Response('FALSE');
                }
                $questionairy = Questionairy::load(reset($questionairyId));
                $uniqueId = $questionairy->get('field_quest_unique_id')->getValue()[0]['value'];
                setcookie('last_questionairy', $uniqueId);
                return new Response($uniqueId);
            }
        }
        return new Response('FALSE');
    }

    /**
     * Createquestionairy.
     */
    private function createQuestionairy() {
        $currentUser = \Drupal::currentUser();

        // Get all the question in order form the taxonomy tree.
        $categories = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('category');
        $returnQuestions = [];
        $entityAnswers = [];
        $totalQuestions = 0;

        $i = 1;
        foreach ($categories as $category) {
            $term = Term::load($category->tid);
            $questions = $term->get('field_category_questions')->getValue();
            $totalQuestions += count($questions);
            $returnQuestions[$category->tid] = [
                'catId' => $category->tid,
                'catName' => $category->name,
                'questions' => [],
            ];

            foreach ($questions as $question) {
                $node = Node::load($question['target_id']);
                $view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');
                $build = $view_builder->view($node, 'default');

                // Create the empty answers
                $current = FALSE;
                if ($i === 1) {
                    $current = TRUE;
                }
                $answer = Answer::create([
                    'uid' => $currentUser->id(),
                    'name' => 'Question-' . $node->id() . ' (Answer)',
                    'field_ans_number' => $i,
                    'field_ans_question' => $question['target_id'],
                    'field_ans_current' => $current,
                    'field_ans_non_aplic' => FALSE,
                ]);
                $answer->setOwnerId($currentUser->id());
                $answer->save();
                $entityAnswers[$answer->id()] = $answer->id();

                $returnQuestions[$category->tid]['questions'][$question['target_id']] = [
                    'render' => render($build),
                    'number' => $i,
                    'answerId' => $answer->id(),
                    'current' => $current,
                ];
                $i++;
            }
        }

        // Create questionairy entity
        $currentDate = new \DateTime('now');
        $currentDateInput = $currentDate->format('Y-m-d\TH:i:s');

        $privacyTree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('privacy_options');
        $questionairy = Questionairy::create([
            'uid' => $currentUser->id(),
            'name' => $currentDate->format('d m Y'),
            'field_quest_created' => $currentDateInput,
            'field_quest_total_questions' => $totalQuestions,
            'field_quest_finished' => FALSE,
            'field_quest_answers' => $entityAnswers,
            'field_quest_privacy' => ['target_id' => $privacyTree[0]->tid],
        ]);
        $uniqueId = uniqid();
        $questionairy->set('field_quest_user', $currentUser->id());


        setcookie('last_questionairy', $uniqueId);
        $questionairy->set('field_quest_unique_id', $uniqueId);
        $questionairy->save();

        return $uniqueId;
    }

    function getUserCreateTitle() {
        $currentUser = \Drupal::currentUser();
        if ($currentUser->id() === 0) {
            return ['#markup' => 'Identificatie'];
        }
        else {
            return ['#markup' => 'Mijn profiel'];
        }
    }
}
