<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class FinactumController.
 */
class FinactumController extends ControllerBase
{

  /**
   * Getcompanybyvte.
   *
   * @return string
   *   Return Hello string.
   */
  public function getCompanyByVTE()
  {
    $isAjax = \Drupal::request()->isXmlHttpRequest();
    if (!$isAjax) {
      return new JsonResponse('FALSE');
    }

    $id = \Drupal::request()->get('id');
    if ($id === NULL) {
      return new JsonResponse('FALSE');
    }
    $url = 'https://api.finactum.io/myEntities';
    $data = ['entities' => 'BE' . $id];
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['APIKEY: ']);

    $response = curl_exec($ch);

    if (!$response) {
      return new JsonResponse('FALSE');
    } else {
      $data = json_decode($response);
      if ($data->totalresults === 0) {
        return new JsonResponse('FALSE');
      }
      $return = [
        'name' => $data->results[0]->name,
        'vte' => $data->results[0]->fte,
        'foundedYear' => (new \DateTime($data->results[0]->foundeddate))->format('Y'),
      ];

      return new JsonResponse($return);
    }
  }
}
