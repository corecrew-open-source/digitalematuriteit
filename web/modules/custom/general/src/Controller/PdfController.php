<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use mikehaertl\wkhtmlto\Pdf;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PdfController.
 */
class PdfController extends ControllerBase {

  /**
   * Makepdf.
   *
   * @return string
   *   Return Hello string.
   */
  public function makePdf() {
      $isAjax = \Drupal::request()->isXmlHttpRequest();
      if ($isAjax) {
          // Get the wkhtml binary path
          $request = \Drupal::request();
          $host = $request->getSchemeAndHttpHost();
          if (strpos($host, 'local') === FALSE) {
              $binaryPath = '/usr/bin/wkhtmltopdf';
          } else {
              $binaryPath = '/usr/local/bin/wkhtmltopdf';
          }

          $html = $request->get('html');
          $name = $request->get('name');
          $pdf = new Pdf([
              'no-outline',           // option without argument
              'encoding' => 'UTF-8',  // option with argument
              // Default page options
              'disable-smart-shrinking',
              'binary' => $binaryPath,
              'user-style-sheet' => getcwd() . '/themes/custom/coretheme/css/print.css',
          ]);

          $newcontent = '<html><head></head><body>' . $html . '</body></html>';
          $pdf->addPage($newcontent);

          $pathName = str_replace(' ', '', $name);
          $pathName = str_replace(':', '-', $pathName);
          $currentUser = \Drupal::currentUser();
          if ($currentUser->id() !== '0') {
              $pathName .= '_' . $currentUser->id();
          }
          $path = $this->getPath('pdf', $pathName);
          $pdf->saveAs($path);

          return new JsonResponse([
              'path' => $path,
              'name' => $pathName,
          ]);
      }
      return new Response('FALSE');
  }

  private function getPath($type, $pathName) {
      return DRUPAL_ROOT . '/sites/default/files/' . $type . '/' . $pathName . '.' . $type;

  }
}
