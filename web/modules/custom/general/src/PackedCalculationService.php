<?php

namespace Drupal\general;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PackedMySQLQueryService.
 */
class PackedCalculationService {
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   *   Full request stack.
   */
  protected $requestStack;

  /**
   * Constructor for the
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(Connection $connection, AccountProxy $currentUser, EntityTypeManagerInterface $entityTypeManager, RequestStack $requestStack) {
    $this->connection = $connection;
    $this->currentUser = $entityTypeManager->getStorage('user')->load($currentUser->id());
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
  }

  /**
   * Calculate the globals.
   *
   * @return array
   *   Associative array containing the needed values.
   */
  public function calculateGlobals() {
    $sectors = $this->currentUser->field_user_sector->referencedEntities();
    $sectorTids = [];
    foreach ($sectors as $sector) {
      $sectorTids[] = $sector->id();
    }

    if (!empty($sectors)) {
      $averages = $this->connection->query('select sum(aa.field_ans_answer_calc_value) as avg from answer__field_ans_answer_calc aa inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = aa.entity_id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join (select entity_id from user__field_user_sector where field_user_sector_target_id IN (:sector)) as us on q.user_id = us.entity_id where qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by q.user_id, q.created', [':sector' => implode(',', $sectorTids)])
        ->fetchAll();
      $totals = $this->connection->query('select avg(averages.average) as avg, max(averages.average) as max, min(averages.average) as min from (select sum(aa.field_ans_answer_calc_value) as average from answer__field_ans_answer_calc aa inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = aa.entity_id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join (select entity_id from user__field_user_sector where field_user_sector_target_id IN (:sector)) as us on q.user_id = us.entity_id where qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by q.user_id, q.created) as averages', [':sector' => implode(',', $sectorTids)])->fetchAll();
    }
    else {
      $averages = $this->connection->query('select sum(aa.field_ans_answer_calc_value) as avg from answer__field_ans_answer_calc aa inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = aa.entity_id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id where qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by q.user_id, q.created')->fetchAll();
      $totals = $this->connection->query('select avg(averages.average) as avg, max(averages.average) as max, min(averages.average) as min from (select sum(aa.field_ans_answer_calc_value) as average from answer__field_ans_answer_calc aa inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = aa.entity_id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id where qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by q.user_id, q.created) as averages')->fetchAll();
    }

    $categoryTerms = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'category',
      ]);

    foreach ($averages as $average) {
      $average->avg = $average->avg / count($categoryTerms);
    }
    $median = $this->calculateMedian($averages);

    return [
      'median' => round($median),
      'average' => round($totals[0]->avg / count($categoryTerms)),
      'min' => round($totals[0]->min / count($categoryTerms)),
      'max' => round($totals[0]->max / count($categoryTerms)),
    ];
  }

  /**
   * Calculate the median for an array of values.
   *
   * @param array $values
   *   The array to values calculate the median for.
   *
   * @return float|int
   *   The calculated median.
   */
  public function calculateMedian(array $values) {
    $count = count($values);
    asort($values);
    $mid = floor(($count - 1) / 2);
    $keys = array_slice(array_keys($values), $mid, (1 === $count % 2 ? 1 : 2));
    $sum = 0;

    foreach ($keys as $key) {
      $sum += $values[$key]->avg;
    }

    if (empty($keys)) {
      return '0';
    }

    return $sum / count($keys);
  }

  /**
   * Fetches all created dates the user filled out the questionairy.
   *
   * @return mixed
   *   Returns all the created dates.
   */
  public function calculateUserCreatedDates() {
    $answerDates = $this->connection->select('questionairy', 'q')
      ->fields('q', ['created', 'id'])
      ->condition('q.user_id', $this->currentUser->id(), '=')
      ->condition('qf.field_quest_finished_value', 1, '=')
//      ->groupBy(['q.created', 'q.id'])
      ->orderBy('q.created', 'desc');

    $answerDates->innerJoin('questionairy__field_quest_finished', 'qf', 'q.id = qf.entity_id');
    $results = $answerDates->execute();
    $results = $results->fetchAll();

    return $results;
  }

  /**
   * Get the average of the questionnaire‎ for the given unix timestamp.
   *
   * @param int $created
   *   The created date.
   *
   * @return int
   *   Returns the rounded average.
   */
  public function calculateUserAverage($created) {
    $average = $this->connection->query('select sum(aa.field_ans_answer_calc_value) as average from answer__field_ans_answer_calc aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id where a.user_id = :uid and a.created = :created and qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\')', [':uid' => $this->currentUser->id(), ':created' => $created])->fetchAll();
    return round($average[0]->average);
  }

  /**
   * Get the average of the questionnaire‎ per category.
   *
   * @param int $created
   *   The created date.
   *
   * @return array
   *   Returns the rounded averages.
   */
  public function calculateUserAveragePerCategory($created) {
    $results = $this->connection->query('select cq.entity_id as category, sum(aa.field_ans_answer_calc_value) as average from answer__field_ans_answer_calc aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id inner join taxonomy_term__field_category_questions as cq on aq.field_ans_question_target_id = cq.field_category_questions_target_id where a.user_id = :uid and q.created = :created and qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by cq.entity_id', [':uid' => $this->currentUser->id(), ':created' => $created])->fetchAll();
    return $this->normalizeResultsPerCategory($results);
  }

  /**
   * Calculate the totals per category.
   *
   * @param int|null $sectorId
   *   The sector to filter the results on.
   * @param int|null $vte
   *   The vte to filter the results on.
   * @param int|null $startYear
   *   The start year to filter the results on.
   * @param int|null $budget
   *   The budget to filter the results on.
   *
   * @return array
   *   An associative array containing min, max, average per category.
   */
  public function calculateTotalsPerCategory($sectorId = NULL, $vte = NULL, $startYear = NULL, $budget = NULL) {
    $subquery = $this->generateUserSubquery($sectorId, $vte, $startYear, $budget);

    if ($subquery) {
      $results = $this->connection->query('select averages.category, avg(averages.average) as avg, max(averages.average) as max, min(averages.average) as min from (select cq.entity_id as category, sum(aa.field_ans_answer_calc_value) as average from answer__field_ans_answer_calc aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id inner join (' . $subquery['subquery'] . ') as us on a.user_id = us.entity_id inner join taxonomy_term__field_category_questions as cq on aq.field_ans_question_target_id = cq.field_category_questions_target_id where qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by a.user_id, a.created, cq.entity_id) as averages group by averages.category', $subquery['args'])->fetchAll();
    }
    else {
      $results = $this->connection->query('select averages.category, avg(averages.average) as avg, max(averages.average) as max, min(averages.average) as min from (select cq.entity_id as category, sum(aa.field_ans_answer_calc_value) as average from answer__field_ans_answer_calc aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id inner join taxonomy_term__field_category_questions as cq on aq.field_ans_question_target_id = cq.field_category_questions_target_id where qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by a.user_id, a.created, cq.entity_id) as averages group by averages.category')->fetchAll();
    }

    return $this->normalizeResultsPerCategory($results);
  }

  /**
   * Calculate the median based on the given category.
   *
   * @param int $categoryId
   *   The category to filter the results on.
   * @param int|null $sectorId
   *   The sector to filter the results on.
   * @param int|null $vte
   *   The vte to filter the results on.
   * @param int|null $startYear
   *   The start year to filter the results on.
   * @param int|null $budget
   *   The budget to filter the results on.
   *
   * @return int
   *   the rounded median..
   */
  public function calculateMedianByCategory($categoryId, $sectorId = NULL, $vte = NULL, $startYear = NULL, $budget = NULL) {
    $subquery = $this->generateUserSubquery($sectorId, $vte, $startYear, $budget);

    if ($subquery) {
      $args = $subquery['args'];
      $args[':category'] = $categoryId;
      $averages = $this->connection->query('select sum(aa.field_ans_answer_calc_value) as avg from answer__field_ans_answer_calc aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id inner join (' . $subquery['subquery'] . ') as us on a.user_id = us.entity_id inner join taxonomy_term__field_category_questions as cq on aq.field_ans_question_target_id = cq.field_category_questions_target_id WHERE cq.entity_id = :category and qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by a.user_id, a.created, cq.entity_id', $args)->fetchAll();
    }
    else {
      $averages = $this->connection->query('select sum(aa.field_ans_answer_calc_value) as avg from answer__field_ans_answer_calc aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id inner join taxonomy_term__field_category_questions as cq on aq.field_ans_question_target_id = cq.field_category_questions_target_id WHERE cq.entity_id = :category and qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by a.user_id, a.created', [':category' => $categoryId])->fetchAll();
    }

    return $this->calculateMedian($averages);
  }

  /**
   * Normalize the results per category.
   *
   * @param $results
   *   Array containing objects returned from MySQL.
   *
   * @return array
   *   Returns an associative array keyed by category.
   */
  public function normalizeResultsPerCategory($results) {
    $return = [];
    foreach ($results as $result) {
      $return[$result->category] = (array) $result;
    }

    return $return;
  }

  /**
   * Get the average of the questionnaire‎ per question.
   *
   * @param int $created
   *   The created date.
   *
   * @return array
   *   Returns the rounded averages.
   */
  public function calculateUserAveragePerQuestion($created) {
    $results = $this->connection->query('select aq.field_ans_question_target_id as question, aa.field_ans_answer_value as answer_value from answer__field_ans_answer aa inner join answer a on a.id = aa.entity_id inner join answer__field_ans_non_aplic as ana on a.id = ana.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id where a.user_id = :uid and q.created = :created and qf.field_quest_finished_value = 1 and ana.field_ans_non_aplic_value = 0 and aa.field_ans_answer_value not in (\'INF\',\'NAN\') group by aa.field_ans_answer_value, aq.field_ans_question_target_id', [':uid' => $this->currentUser->id(), ':created' => $created])->fetchAll();
    return $this->normalizeResultsPerQuestion($results);
  }

  /**
   * Get the non applicable values for all questions.
   *
   * @param int $created
   *   The created date.
   *
   * @return array
   *   Returns the rounded averages.
   */
  public function calculateUserNANPerQuestion($created) {
    $results = $this->connection->query('select aq.field_ans_question_target_id as question from answer a inner join answer__field_ans_non_aplic as ana on a.id = ana.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id where a.user_id = :uid and q.created = :created and qf.field_quest_finished_value = 1 and ana.field_ans_non_aplic_value = 1 group by aq.field_ans_question_target_id', [':uid' => $this->currentUser->id(), ':created' => $created])->fetchAll();
    return $this->normalizeResultsPerQuestion($results);
  }


  /**
   * Calculate the totals per question.
   *
   * @param int|null $sectorId
   *   The sector to filter the results on.
   * @param int|null $vte
   *   The vte to filter the results on.
   * @param int|null $startYear
   *   The start year to filter the results on.
   * @param int|null $budget
   *   The budget to filter the results on.
   *
   * @return array
   *   An associative array containing min, max, average per question.
   */
  public function calculateTotalsPerQuestion($sectorId = NULL, $vte = NULL, $startYear = NULL, $budget = NULL) {
    $subquery = $this->generateUserSubquery($sectorId, $vte, $startYear, $budget);

    if ($subquery) {
      $args = $subquery['args'];
      $results = $this->connection->query('select averages.question, avg(averages.average) as avg, max(averages.average) as max, min(averages.average) as min from (select aq.field_ans_question_target_id as question, avg(aa.field_ans_answer_value) as average from answer__field_ans_answer aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id inner join (' . $subquery['subquery'] . ') as us on a.user_id = us.entity_id where qf.field_quest_finished_value = 1 and aa.field_ans_answer_value not in (\'INF\',\'NAN\') group by a.user_id, a.created, aq.field_ans_question_target_id) as averages group by averages.question', $args)->fetchAll();
    }
    else {
      $results = $this->connection->query('select averages.question, avg(averages.average) as avg, max(averages.average) as max, min(averages.average) as min from (select aq.field_ans_question_target_id as question, avg(aa.field_ans_answer_value) as average from answer__field_ans_answer aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id where qf.field_quest_finished_value = 1 and aa.field_ans_answer_value not in (\'INF\',\'NAN\') group by a.user_id, a.created, aq.field_ans_question_target_id) as averages group by averages.question')->fetchAll();
    }

    return $this->normalizeResultsPerQuestion($results);
  }

  /**
   * Calculate the median based on the given question.
   *
   * @param int $questionId
   *   The question to filter the results on.
   * @param int|null $sectorId
   *   The sector to filter the results on.
   * @param int|null $vte
   *   The vte to filter the results on.
   * @param int|null $startYear
   *   The start year to filter the results on.
   * @param int|null $budget
   *   The budget to filter the results on.
   *
   * @return int
   *   the rounded median..
   */
  public function calculateMedianByQuestion($questionId, $sectorId = NULL, $vte = NULL, $startYear = NULL, $budget = NULL) {
    $subquery = $this->generateUserSubquery($sectorId, $vte, $startYear, $budget);

    if ($subquery) {
      $args = $subquery['args'];
      $args[':question'] = $questionId;
      $averages = $this->connection->query('select avg(aa.field_ans_answer_value) as avg from answer__field_ans_answer aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id inner join (' . $subquery['subquery'] . ') as us on a.user_id = us.entity_id WHERE aq.field_ans_question_target_id = :question and qf.field_quest_finished_value = 1 and aa.field_ans_answer_value not in (\'INF\',\'NAN\') group by a.user_id, a.created', $args)->fetchAll();
    }
    else {
      $averages = $this->connection->query('select avg(aa.field_ans_answer_value) as avg from answer__field_ans_answer aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id WHERE aq.field_ans_question_target_id = :question and qf.field_quest_finished_value = 1 and aa.field_ans_answer_value not in (\'INF\',\'NAN\') group by a.user_id, a.created', [':question' => $questionId])->fetchAll();
    }

    return $this->calculateMedian($averages);
  }

  /**
   * Normalize the results per question.
   *
   * @param $results
   *   Array containing objects returned from MySQL.
   *
   * @return array
   *   Returns an associative array keyed by question.
   */
  public function normalizeResultsPerQuestion($results) {
    $return = [];
    foreach ($results as $result) {
      $return[$result->question] = (array) $result;
    }

    return $return;
  }

  /**
   * Generate the user subquery based on the given values.
   *
   * @param int|null $sectorId
   *   The sector to filter the results on.
   * @param int|null $vte
   *   The vte to filter the results on.
   * @param int|null $startYear
   *   The start year to filter the results on.
   * @param int|null $budget
   *   The budget to filter the results on.
   *
   * @return array|bool
   *   Array containing the fully build subquery and the needed args.
   */
  protected function generateUserSubquery($sectorId = NULL, $vte = NULL, $startYear = NULL, $budget = NULL) {
    if (($sectorId && $sectorId != 'all') || ($vte && $vte != 'all') || ($startYear && $startYear != 'all') || ($budget && $budget != 'all')) {
      $isStart = TRUE;
      $first = '';
      $where = '';
      $subquery = '';
      $args = [];

      if ($sectorId && $sectorId != 'all') {
        if (!is_array($sectorId)) {
          if ($parents = $this->entityTypeManager->getStorage('taxonomy_term')
            ->loadParents($sectorId)) {
            $subquery = 'select fus.entity_id from user__field_user_subsector as fus';
            $where = 'where fus.field_user_subsector_target_id = :sector';
            $args[':sector'] = $sectorId;
            $first = 'fus';
            $isStart = FALSE;
          }
          else {
            $subquery = 'select fus.entity_id from user__field_user_sector as fus';
            $where = 'where fus.field_user_sector_target_id = :sector';
            $args[':sector'] = $sectorId;
            $first = 'fus';
            $isStart = FALSE;
          }
        }
        else {
          $sectorTids = [];
          foreach ($sectorId as $sector) {
            $sectorTids[] = $sector->id();
          }

          $subquery = 'select fus.entity_id from user__field_user_sector as fus';
          $where = 'where fus.field_user_sector_target_id = :sector';
          $args[':sector'] = implode(',', $sectorTids);
          $first = 'fus';
          $isStart = FALSE;
        }
      }

      if ($vte && $vte != 'all') {
        if ($isStart) {
          $subquery = 'select fuv.entity_id from user__field_user_vte as fuv';
          $where = 'where fuv.field_user_vte_target_id = :vte';
          $first = 'fuv';
          $isStart = FALSE;
        }
        else {
          $subquery .= ' inner join user__field_user_vte as fuv on fus.entity_id = fuv.entity_id';
          $where .= ' and fuv.field_user_vte_target_id = :vte';
        }

        $args[':vte'] = $vte;
      }

      if ($startYear && $startYear != 'all') {
        if ($isStart) {
          $subquery = 'select fusy.entity_id from user__field_user_startyear as fusy';
          $where = 'where fusy.field_user_startyear_target_id = :start_year';
          $first = 'fusy';
          $isStart = FALSE;
        }
        else {
          $subquery .= ' inner join user__field_user_startyear as fusy on ' . $first . '.entity_id = fusy.entity_id';
          $where .= ' and fusy.field_user_startyear_target_id = :start_year';
        }

        $args[':start_year'] = $startYear;
      }

      if ($budget && $budget != 'all') {
        if ($isStart) {
          $subquery = 'select fub.entity_id from user__field_user_budget as fub';
          $where = 'where fub.field_user_budget_target_id = :budget';
          $first = 'fub';
          $isStart = FALSE;
        }
        else {
          $subquery .= ' inner join user__field_user_budget as fub on ' . $first . '.entity_id = fub.entity_id';
          $where .= ' and fub.field_user_budget_target_id = :budget';
        }

        $args[':budget'] = $budget;
      }

      return [
        'subquery' => $subquery . ' ' . $where,
        'args' => $args,
      ];
    }

    return FALSE;
  }

  /**
   * Gets the answer descriptions filled out by the user.
   *
   * @param int $created
   *   The created date.
   *
   * @return array
   *   Returns descriptions.
   */
  public function getUserDescriptions($created) {
    $results = $this->connection->query('select ad.field_ans_desciption_value as description, aq.field_ans_question_target_id as question from answer__field_ans_desciption ad inner join answer a on a.id = ad.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id where a.user_id = :uid and a.created = :created', [':uid' => $this->currentUser->id(), ':created' => $created])->fetchAll();
    return $this->normalizeResultsPerQuestion($results);
  }

  /**
   * Get the questionairy values for an anonymous user.
   */
  public function getAnonymousUserValues() {
    $request = $this->requestStack->getCurrentRequest();
    $cookieString = $request->cookies->get('last_questionairy');

    $res = $this->connection->query('select entity_id from questionairy__field_quest_unique_id where field_quest_unique_id_value = :hash', [':hash' => $cookieString])->fetchAll();
    $qid = reset($res)->entity_id;

    if (!empty($qid)) {
      $userDescriptions = $this->connection->query('select ad.field_ans_desciption_value as description, aq.field_ans_question_target_id as question from answer__field_ans_desciption ad inner join answer__field_ans_question as aq on ad.entity_id = aq.entity_id inner join questionairy__field_quest_answers as fqa on ad.entity_id = fqa.field_quest_answers_target_id where fqa.entity_id = :qid group by question, description', [':qid' => $qid])
        ->fetchAll();
      $tooltips = $this->normalizeResultsPerQuestion($userDescriptions);

      $userTotalAverage = $this->connection->query('select sum(aa.field_ans_answer_calc_value) as average from answer__field_ans_answer_calc aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id where qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') and q.id = :qid', [':qid' => $qid])
        ->fetchAll();

      $categoryTerms = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadByProperties([
          'vid' => 'category',
        ]);

      $userTotalAverage = round($userTotalAverage[0]->average / count($categoryTerms));

      $userAveragePerCategory = $this->connection->query('select cq.entity_id as category, sum(aa.field_ans_answer_calc_value) as average from answer__field_ans_answer_calc aa inner join answer a on a.id = aa.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id inner join taxonomy_term__field_category_questions as cq on aq.field_ans_question_target_id = cq.field_category_questions_target_id where q.id = :qid and qf.field_quest_finished_value = 1 and aa.field_ans_answer_calc_value not in (\'INF\',\'NAN\') group by cq.entity_id', [':qid' => $qid])->fetchAll();
      $userAveragePerCategory = $this->normalizeResultsPerCategory($userAveragePerCategory);

      $userAveragePerQuestion = $this->connection->query('select aq.field_ans_question_target_id as question, aa.field_ans_answer_value as answer_value from answer__field_ans_answer aa inner join answer a on a.id = aa.entity_id inner join answer__field_ans_non_aplic as ana on a.id = ana.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id where q.id = :qid and qf.field_quest_finished_value = 1 and ana.field_ans_non_aplic_value = 0 and aa.field_ans_answer_value not in (\'INF\',\'NAN\') group by aa.field_ans_answer_value, aq.field_ans_question_target_id', [':qid' => $qid])->fetchAll();
      $userAveragePerQuestion = $this->normalizeResultsPerQuestion($userAveragePerQuestion);

      $nanPerQuestion = $this->connection->query('select aq.field_ans_question_target_id as question from answer a inner join answer__field_ans_non_aplic as ana on a.id = ana.entity_id inner join questionairy__field_quest_answers qa on qa.field_quest_answers_target_id = a.id inner join questionairy q on q.id = qa.entity_id inner join questionairy__field_quest_finished qf on q.id = qf.entity_id inner join answer__field_ans_question as aq on a.id = aq.entity_id where q.id = :qid and qf.field_quest_finished_value = 1 and ana.field_ans_non_aplic_value = 1 group by aq.field_ans_question_target_id', [':qid' => $qid])->fetchAll();
      $nanPerQuestion = $this->normalizeResultsPerQuestion($nanPerQuestion);

      return [
        'tooltips' => $tooltips,
        'total' => $userTotalAverage,
        'category' => $userAveragePerCategory,
        'question' => $userAveragePerQuestion,
        'nan' => $nanPerQuestion,
      ];
    }

    return FALSE;
  }
}
