<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Answer entities.
 *
 * @ingroup general
 */
interface AnswerInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Answer name.
   *
   * @return string
   *   Name of the Answer.
   */
  public function getName();

  /**
   * Sets the Answer name.
   *
   * @param string $name
   *   The Answer name.
   *
   * @return \Drupal\general\Entity\AnswerInterface
   *   The called Answer entity.
   */
  public function setName($name);

  /**
   * Gets the Answer creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Answer.
   */
  public function getCreatedTime();

  /**
   * Sets the Answer creation timestamp.
   *
   * @param int $timestamp
   *   The Answer creation timestamp.
   *
   * @return \Drupal\general\Entity\AnswerInterface
   *   The called Answer entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Answer published status indicator.
   *
   * Unpublished Answer are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Answer is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Answer.
   *
   * @param bool $published
   *   TRUE to set this Answer to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\general\Entity\AnswerInterface
   *   The called Answer entity.
   */
  public function setPublished($published);

}
