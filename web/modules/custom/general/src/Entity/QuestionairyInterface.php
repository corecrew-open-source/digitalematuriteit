<?php

namespace Drupal\general\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Questionairy entities.
 *
 * @ingroup general
 */
interface QuestionairyInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Questionairy name.
   *
   * @return string
   *   Name of the Questionairy.
   */
  public function getName();

  /**
   * Sets the Questionairy name.
   *
   * @param string $name
   *   The Questionairy name.
   *
   * @return \Drupal\general\Entity\QuestionairyInterface
   *   The called Questionairy entity.
   */
  public function setName($name);

  /**
   * Gets the Questionairy creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Questionairy.
   */
  public function getCreatedTime();

  /**
   * Sets the Questionairy creation timestamp.
   *
   * @param int $timestamp
   *   The Questionairy creation timestamp.
   *
   * @return \Drupal\general\Entity\QuestionairyInterface
   *   The called Questionairy entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Questionairy published status indicator.
   *
   * Unpublished Questionairy are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Questionairy is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Questionairy.
   *
   * @param bool $published
   *   TRUE to set this Questionairy to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\general\Entity\QuestionairyInterface
   *   The called Questionairy entity.
   */
  public function setPublished($published);

}
