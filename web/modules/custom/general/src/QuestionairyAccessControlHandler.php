<?php

namespace Drupal\general;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Questionairy entity.
 *
 * @see \Drupal\general\Entity\Questionairy.
 */
class QuestionairyAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\general\Entity\QuestionairyInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished questionairy entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published questionairy entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit questionairy entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete questionairy entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add questionairy entities');
  }

}
