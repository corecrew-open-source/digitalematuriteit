<?php
/**
 * @file
 * Contains \Drupal\general\Routing\RouteSubscriber.
 */

namespace Drupal\general\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

    /**
     * {@inheritdoc}
     */
    public function alterRoutes(RouteCollection $collection) {
        if ($route = $collection->get('user.login')) {
            $route->setDefault('_title', 'Identificatie');
        }
    }
}