<?php

namespace Drupal\general\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * ApiV1QuestionairyResource class.
 *
 * @RestResource(
 *   id = "questionairy_resource",
 *   entity_type = "questionairy",
 *   entity_bundle = "questionairy",
 *   label = @Translation("List all the questionairies."),
 *   uri_paths = {
 *     "canonical" = "/api/v1/questionairy"
 *   }
 * )
 */
class ApiV1QuestionairyResource extends ResourceBase {

  /**
   * Entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Full request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Entity bundle.
   *
   * @var string
   */
  protected $entityBundle;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   *
   * @throws \Exception
   *   When the definition is incomplete.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, EntityTypeManagerInterface $entity_manager, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->entityManager = $entity_manager;
    $this->requestStack = $request_stack;

    if (!$plugin_definition['entity_type']) {
      throw new \Exception("Annotation 'entity_type' definition is missing from " . get_called_class());
    }

    $this->entityType = $plugin_definition['entity_type'];
    $this->entityBundle = $plugin_definition['entity_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Handle the extracting of the data for a single entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to provide the data for.
   * @param array $categories
   *   Array containing category data.
   *
   * @return array
   *   The data that should be added to the api.
   */
  public function prepareJsonData(EntityInterface $entity, array $categories) {
    $userId = empty($entity->user_id->getString()) ? 0 : $entity->user_id->getString();
    $sector = '';

    if (!empty($userId)) {
      $user = User::load($userId);

      $sector = [];
      $sectors = $user->field_user_sector->referencedEntities();
      foreach ($sectors as $sectorEntity) {
        $sector[] = $sectorEntity->label();
      }
    }

    $answers = $entity->field_quest_answers->referencedEntities();
    foreach ($answers as $answerEntity) {
      if (!empty($answerEntity->field_ans_non_aplic->getString())) {
        $answerResult = 'NVT';
        $rawAnswerResult = 'NVT';
      }
      else {
        $answerResult = round($answerEntity->field_ans_answer_calc->getString());
        $rawAnswerResult = $answerEntity->field_ans_answer->getString();
      }

      $question = reset($answerEntity->field_ans_question->referencedEntities());
      foreach ($categories as $id => $category) {
        if (in_array($question->id(), $category['questions'])) {
          $categories[$id]['questions'][$question->id()] = [
            'Title vraag' => $question->label(),
            'Calculated value answer' => $answerResult,
            'Raw answer value' => $rawAnswerResult,
          ];
        }
      }
    }

    $i=5;
    return [
      'ID' => $entity->id(),
      'UID' => $userId,
      'Timestamp' => $entity->getCreatedTime(),
      'Sector' => $sector,
      'Answers' => $categories,
    ];
  }

  /**
   * Load all the entities valid for this request.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   All the items for the this request.
   */
  protected function getEntitiesByRequest($countElements = FALSE) {
    // Filter based on params.
    $currentRequest = $this->requestStack->getCurrentRequest();
    $filter_params = $currentRequest->query->get('filter', []);
    $sort_params = $currentRequest->query->get('sort', []);
    $limit = !empty($currentRequest->query->get('limit')) ? $currentRequest->query->get('limit') : 20;
    $offset = !empty($currentRequest->query->get('offset')) ? $currentRequest->query->get('offset') : 0;

    // Prepare query.
    $query = $this->entityManager->getStorage('questionairy')->getQuery();

    // Filter query.
    foreach ($filter_params as $field => $value) {
      if ($field == 'startdate') {
        $query->condition('created', $value, '>=');
      }

      if ($field == 'enddate') {
        $query->condition('created', $value, '<=');
      }
    }

    // Sort query.
    foreach ($sort_params as $sort_field => $direction) {
      $query->sort($sort_field, $direction);
    }

    // Run query.
    $query->accessCheck(FALSE);
    if ($countElements) {
      return $query->count()->execute();
    }
    else {
      $query->range($offset, $limit);

      try {
        $entity_ids = $query->execute();
      }
      catch (\Exception $e) {
        throw new BadRequestHttpException(t('The following is wrong in the given query.') . $e->getMessage());
      }

      // Get entities.
      $entity_storage = $this->entityManager->getStorage($this->entityType);

      return $entity_storage->loadMultiple($entity_ids);
    }
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    $entities = $this->getEntitiesByRequest();

    $output = [];

    $categories = [];
    $categoryTerms = $this->entityManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'category',
      ]);

    foreach ($categoryTerms as $categoryTerm) {
      $categoryQuestions = $categoryTerm->field_category_questions->referencedEntities();
      $categories[$categoryTerm->id()] = [
        'Categorie vraag' => $categoryTerm->label(),
        'questions' => [],
      ];

      foreach ($categoryQuestions as $categoryQuestion) {
        $categories[$categoryTerm->id()]['questions'][$categoryQuestion->id()] = $categoryQuestion->id();
      }
    }

    foreach ($entities as $entity) {
      $output[$entity->id()] = $this->prepareJsonData($entity, $categories);
    }

    $totalCount = $this->getEntitiesByRequest(TRUE);

    $cacheMetadata = new CacheableMetadata();
    $cacheMetadata->setCacheContexts(['url']);
    $cacheMetadata->setCacheMaxAge(3600);
    $cacheMetadata->setCacheTags(['node_list']);

    $response = new CacheableJsonResponse($output, 200, ['X-Total-Count' => $totalCount]);
    $response->addCacheableDependency($cacheMetadata);

    return $response;
  }

}
