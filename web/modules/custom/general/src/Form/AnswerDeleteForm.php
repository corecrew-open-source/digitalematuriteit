<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Answer entities.
 *
 * @ingroup general
 */
class AnswerDeleteForm extends ContentEntityDeleteForm {


}
