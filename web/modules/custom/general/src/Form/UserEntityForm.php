<?php

namespace Drupal\general\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class UserForm.
 */
class UserEntityForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_entity_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $currentUser = \Drupal::currentUser();
    if ($currentUser->id() === 0) {
        $form['title'] = [
            '#markup' => '<ul><li><a href="/user/login">Aanmelden</a></li><li><a class="is-active" href="/general/user/register">Inschrijven</a></li></ul>',
        ];
    }

    $form['companynumber'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Ondernemingsnr.'),
        '#attributes' => [
            'placeholder' => '0802134567',
        ],
        '#required' => 'required',
    ];

    $form['company'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Organisatie'),
        '#required' => 'required',
    ];

    $form['sector'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Sector'),
    ];

    $sectors = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('sector');
    $form['hidden'] = [
        '#type' => 'hidden',
        '#value' => 'sector-' . $sectors[0]->tid,
    ];

    foreach ($sectors as $sector) {
        if ($sector->tid !== '1') {
            if ($sector->parents[0] === '0') {
                $form['sector']['sector-' . $sector->tid] = [
                    '#type' => 'checkboxes',
                    '#title' => $sector->name,
                    '#options' => [],
                ];
            } else {
                foreach ($sector->parents as $parent) {
                    $form['sector']['sector-' . $parent]['#options'][$sector->tid] = $sector->name;
                }
            }
        }
    }

    $form['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Jouw naam'),
        '#required' => 'required',
    ];

    $form['email'] = [
        '#type' => 'email',
        '#title' => $this->t('E-mail'),
        '#required' => 'required',
    ];

    if (!$currentUser->isAnonymous()) {
        $form['email']['#description'] = '<a href="/user/password">wachtwoord opnieuw instellen</a>';
    }

    $form['vte'] = [
        '#type' => 'select',
        '#title' => $this->t("VTE's"),
        '#options' => [],
        '#required' => 'required',
    ];

    $form['startyear'] = [
        '#type' => 'select',
        '#title' => $this->t('Oprichtingsjaar'),
        '#options' => [],
        '#required' => 'required',
    ];

    $form['budget'] = [
        '#type' => 'select',
        '#title' => $this->t('Werkingsbudget'),
        '#options' => [],
        '#required' => 'required',
    ];

    $selectsArr = [
      'vte' => 'vte',
      'budget' => 'budget',
      'startyear' =>  'start_year'
    ];

    foreach ($selectsArr as $element => $voc) {
        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($voc);
        foreach ($terms as $term) {
            $form[$element]['#options'][$term->tid] = $term->name;
        }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#prefix' => '<div class="form-actions">',
      '#value' => $this->t('Inschrijven'),
    ];

    if ($currentUser->id() !== 0) {
        $form['submit']['#value'] = $this->t('Opslaan');
    }


    if ($currentUser->id() === 0) {
        $form['privacy'] = [
            '#type' => 'checkbox',
            '#required' => 'required',
            '#title' => $this->t('Ik ga akkoord met de <a href="/privacy-policy" target="_blank">privacyvoorwaarden</a>'),
            '#suffix' => '</div>',
        ];
    }

    // Set default values
    if ($currentUser->id() !== 0) {
        $currentUser = User::load($currentUser->id());
        $vars = [
            'companynumber',
            'company',
            'name',
        ];
        foreach ($vars as $element) {
            if (isset($currentUser->get('field_user_' . $element)->getValue()[0])) {
                $form[$element]['#default_value'] = $currentUser->get('field_user_' . $element)->getValue()[0]['value'];
            }
        }

        foreach ($selectsArr as $key => $element) {
            if (isset($currentUser->get('field_user_' . $key)->getValue()[0])) {
                $form[$key]['#default_value'] = $currentUser->get('field_user_' . $key)->getValue()[0]['target_id'];
            }
        }

        if (isset($currentUser->get('mail')->getValue()[0])) {
            $form['email']['#default_value'] = $currentUser->get('mail')->getValue()[0]['value'];
        }

        if (count($currentUser->get('field_user_sector')->getValue()) > 0) {
            foreach ($currentUser->get('field_user_sector')->getValue() as $sector) {
                $userSector = $sector['target_id'];
                foreach ($currentUser->get('field_user_subsector')->getValue() as $subsector) {
                    $userSubSector = $subsector['target_id'];
                    $form['sector']['sector-' . $userSector]['#default_value'][$userSubSector] = $userSubSector;
                }
            }
        }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $currentUser = \Drupal::currentUser();
    if ($currentUser->id() === 0) {
        $user = user_load_by_mail($form_state->getValue('email'));
        if ($user !== FALSE) {
            $form_state->setError($form['email'], t('Er bestaat al een gebruiker met dit mailadres'));
        }

        $query = \Drupal::entityQuery('user')->condition('field_user_companynumber', $form_state->getValue('companynumber'));
        $organisatie = $query->execute();

        if (count($organisatie) > 0) {
            $userId = reset($organisatie);
            $user = User::load($userId);
            $form_state->setError($form['companynumber'], t('Er bestaat al een organisatie met dit nummer. Deze is gekoppeld aan ' . $user->getEmail()));
        }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      $values = $form_state->getValues();
      $sectors = [];
      $subsectors = [];
      foreach ($values as $key => $value) {
          if (strpos($key, 'sector') !== FALSE) {
              foreach ($value as $subsectorid => $on) {
                  if ($on !== 0) {
                      $sectors[str_replace('sector-', '', $key)] = str_replace('sector-', '', $key);
                      $subsectors[$subsectorid] = $subsectorid;
                  }
              }
          }
      }

      $userValues = [
          'status' => 1,
          'name' => $values['email'],
          'mail' => $values['email'],
          'field_user_companynumber' => $values['companynumber'],
          'field_user_company' => $values['company'],
          'field_user_name' => $values['name'],
          'field_user_vte' => ['target_id' => $values['vte']],
          'field_user_startyear' => ['target_id' => $values['startyear']],
          'field_user_budget' => ['target_id' => $values['budget']],
          'field_user_sector' => $sectors,
          'field_user_subsector' => $subsectors,
      ];

      $currentUser = \Drupal::currentUser();
      if ($currentUser->id() !== 0) {
          $user = User::load($currentUser->id());
          foreach ($userValues as $key => $value) {
              $user->set($key, $value);
          }
          drupal_set_message($this->t('Jouw profiel is succesvol opgeslagen'));
          $user->save();

      } else {
          drupal_set_message($this->t('Er is een e-mail verstuurd naar @mail voor het vervolledigen van je registratie', ['@mail' => $values['email']]));
          $userValues['pass'] = user_password();
          $user = User::create($userValues);
          $user->save();

          _user_mail_notify('register_no_approval_required', $user);
      }
  }

}
