<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Radios;
use Drupal\Core\Session\AccountProxy;
use Drupal\general\Entity\Questionairy;
use Drupal\general\PackedCalculationService;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the results forms.
 */
class ResultsForm extends FormBase
{

    /**
     * The mysql service.
     *
     * @var \Drupal\general\PackedCalculationService
     */
    protected $calculationService;

    /**
     * The entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * The current user.
     *
     * @var \Drupal\Core\Session\AccountInterface
     */
    protected $currentUser;

    /**
     * Constructs a new ResultsForm.
     *
     * @param \Drupal\Core\Session\AccountProxy $currentUser
     *   The current user.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
     *   The entity type manager.
     * @param \Drupal\general\PackedCalculationService $calculationService
     *   The mysql query service.
     */
    public function __construct(AccountProxy $currentUser, EntityTypeManagerInterface $entityTypeManager, PackedCalculationService $calculationService)
    {
        $this->calculationService = $calculationService;
        $this->currentUser = $entityTypeManager->getStorage('user')->load($currentUser->id());
        $this->entityTypeManager = $entityTypeManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('current_user'),
            $container->get('entity_type.manager'),
            $container->get('general.calculation_service')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'packed_results_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form_state->set('globalValues', $this->calculationService->calculateGlobals());
        $form['#attributes']['id'] = ['main-form-content'];
        $form['container'] = [
            '#type' => 'container',
            '#attributes' => [
                'class' => 'form-header'
            ],
        ];

        $options = [];
        $ids = [];
        $bool = TRUE;
        if (!$this->currentUser->isAnonymous()) {
            foreach ($this->calculationService->calculateUserCreatedDates() as $date) {
                $options[$date->created] = date('d m Y - H:i', $date->created);
                $ids[$date->created] = $date->id;
            }

            if (count($options) === 0) {
                $bool = FALSE;
                $form['intro_text'] = [
                    '#markup' => '<div class="no-results"><p>Er zijn nog geen resultaten beschikbaar. Gelieve eerst een evalutatie te vervolledigen.</p><p><a href="#" class="button start-tool">Start de tool</a> </p></div>'
                ];

            } else {
                $form['container']['start_date'] = [
                    '#type' => 'select',
                    '#title' => t('Results'),
                    '#options' => $options,
                    '#ajax' => [
                        'callback' => '::replaceMain',
                        'wrapper' => 'main-form-content',
                        'event' => 'change',
                        'method' => 'replace',
                        'effect' => 'fade',
                    ],
                ];
            }
        }

        if ($bool) {
            $form['container']['type_switcher']['median'] = [
                '#type' => 'html_tag',
                '#attributes' => [
                    'class' => 'median'
                ],
                '#tag' => 'a',
                '#value' => t('Mediaan'),
            ];

            $form['container']['type_switcher']['av'] = [
                '#type' => 'html_tag',
                '#attributes' => [
                    'class' => 'average'
                ],
                '#tag' => 'a',
                '#value' => t('Average'),
            ];

            $form['intro_text'] = [
                '#type' => 'html_tag',
                '#tag' => 'p',
                '#value' => t('Hieronder zie je de totaalscore en subscores van je organisatie. Je kunt telkens vergelijken met het gemiddelde of de mediaan, de hoogste en de laagste score van anderen. Verderop kun je zelf een aantal filters instellen om je score te vergelijken met die van een zelfgekozen subset van culturele organisaties. Je kunt ook je huidige resultaten vergelijken met eerdere resultaten.'),
            ];

            $form += $this->generateForm($options, $form_state, $ids);
        }


        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $formState)
    {
        // Do nothing, everything is handled by AJAX.
    }

    /**
     * Generate the top block.
     *
     * @param array $options
     *  All date options.
     * @param FormStateInterface $form_state
     *  The form state.
     *
     * @return array
     *   Renderable form array.
     */
    protected function generateForm($options, FormStateInterface $form_state, $ids)
    {
        if (!$this->currentUser->isAnonymous()) {
            if (!empty($form_state->get('start_date'))) {
                $created = $form_state->get('start_date');
            } elseif (!empty($form_state->getValue('start_date'))) {
                $created = $form_state->getValue('start_date');
            } else {
                $created = key($options);
            }

            if (!empty($form_state->get('old_date'))) {
                $createdOld = $form_state->get('old_date');
            } elseif (!empty($form_state->getValue('old_date'))) {
                $createdOld = $form_state->getValue('old_date');
            } else {
                $createdOld = key(array_slice($options, 1, 1, TRUE));
            }
        }

        $subsector = $form_state->getValue('subsector');
        $vte = $form_state->getValue('vte');
        $start_year = $form_state->getValue('start_year');
        $budget = $form_state->getValue('budget');
        $categories = $this->getCategories();

        $globalValues = $form_state->get('globalValues');
        $sector = $this->currentUser->field_user_sector->referencedEntities();
        if (!$this->currentUser->isAnonymous()) {
          $myAveragePerCategory = $this->calculationService->calculateUserAveragePerCategory($created);
          $myAveragePerCategoryOld = $this->calculationService->calculateUserAveragePerCategory($createdOld);
          $myAverage = $this->calculationService->calculateUserAverage($created) / count($myAveragePerCategory);
          $questionDescriptions = $this->calculationService->getUserDescriptions($created);
          $nanPerQuestion = $this->calculationService->calculateUserNANPerQuestion($created);
        } else {
          $anonymousValues = $this->calculationService->getAnonymousUserValues();
          $myAverage = !empty($anonymousValues['total']) ? $anonymousValues['total'] : '0';
          $myAveragePerCategory = $anonymousValues['category'];
          $questionDescriptions = !empty($anonymousValues['tooltips']) ? $anonymousValues['tooltips'] : [];
          $nanPerQuestion = !empty($anonymousValues['nan']) ? $anonymousValues['nan'] : [];
        }

        $element['main_form_content'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => 'main-form-content'
            ],
        ];

        $element['main_form_content']['scores'] = [
            'general' => [
                '#markup' => '<h2>' . t('Je algemene score') . ': ' . round($myAverage) . '%</h2>',
            ],

        ];
        $element['main_form_content']['scores']['element'] = [
            '#type' => 'html_tag',
            '#tag' => 'p',
            'average' => [
                '#type' => 'html_tag',
                '#tag' => 'span',
                '#value' => t('Gemiddelde score') . ': ' . $globalValues['average'] . '%',
            ],
            'median' => [
                '#type' => 'html_tag',
                '#tag' => 'span',
                '#value' => t('Mediaan score') . ': ' . $globalValues['median'] . '%',
            ],
            'minimal' => [
                '#type' => 'html_tag',
                '#tag' => 'span',
                '#value' => t('Laagste score') . ': ' . $globalValues['min'] . '%',
            ],
            'maximal' => [
                '#type' => 'html_tag',
                '#tag' => 'span',
                '#value' => t('Hoogste score') . ': ' . $globalValues['max'] . '%',
            ],
        ];
        $element['main_form_content']['top_table_container'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => 'top-table'
            ],
        ];
        $element['main_form_content']['top_table_container']['top_table'] = [
            '#type' => 'table',
            '#header' => [
                '',
                t('Score'),
                t('Gem.'),
                t('Med.'),
                t('Laagste'),
                t('Hoogste'),
            ],
        ];

        $element['main_form_content']['top_table_container']['top_diagram'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => 'top-diagram'
            ],
        ];

        $element['main_form_content']['top_table_container']['top_diagram']['top_chart'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => ['top-chart-wrapper'],
                'style' => ['width: 200%;'],
            ],
            '#attached' => [
                'library' => [
                    'general/chartjs.chart',
                    'general/general.chart',
                ],
                'drupalSettings' => [
                    'generateChartTop' => [],
                ],
            ],
            'html' => [
                '#type' => 'html_tag',
                '#tag' => 'canvas',
                '#attributes' => [
                    'id' => ['top-chart'],
                ],
            ],
        ];

        $element['main_form_content']['top_table_container']['top_diagram']['top_chart_2'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => ['top-chart2-wrapper'],
                'style' => ['width: 200%;'],
            ],
            'html' => [
                '#type' => 'html_tag',
                '#tag' => 'canvas',
                '#attributes' => [
                    'id' => ['top-chart2'],
                ],
            ],
        ];


        $element['main_form_content']['top_table_container']['top_diagram']['top_chart_legend'] = [
            'general' => [
                '#markup' => '<div class="legend"><span>'.t('Je score').'</span><br><span>'.t('Gemiddelde score van alle respondenten').'</span></div>',
            ],
        ];


        $element['main_form_content']['middle'] = [
            '#type' => 'container',
            'title' => [
                '#markup' => '<h2>'.t('Je score selectief vergelijken met de sector').'</h2>',
            ],
            'selects' => [
                '#type' => 'container',
            ],
        ];

        $subsectorTree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('sector');
        $subsectorOptions = ['all' => t('Alle')];
        foreach ($subsectorTree as $subsectorOption) {
            if ($subsectorOption->tid !== '1') {
                if (empty($subsectorOption->parents) || $subsectorOption->parents[0] == '0') {
                    $subsectorOptions[$subsectorOption->tid] = $subsectorOption->name;
                }
/*                else {
                    $subsectorOptions[$subsectorOption->tid] = '- ' . $subsectorOption->name;
                }*/
            }
        }

        $sectorDefaultValue = !empty($sector) ? reset($sector)->id() : 'all';
        $element['main_form_content']['middle']['selects']['subsector'] = [
            '#type' => 'select',
            '#title' => t('Sector'),
            '#options' => $subsectorOptions,
            '#default_value' => $sectorDefaultValue,
            '#ajax' => [
                'callback' => '::replaceMiddleTable',
                'wrapper' => 'middle-table',
                'event' => 'change',
                'method' => 'replace',
                'effect' => 'fade',
            ],
        ];

        $vteTree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('vte');
        $vteOptions = ['all' => t('Alle')];
        foreach ($vteTree as $vteOption) {
            $vteOptions[$vteOption->tid] = $vteOption->name;
        }
        $element['main_form_content']['middle']['selects']['vte'] = [
            '#type' => 'select',
            '#title' => t('Volgens VTE\'s'),
            '#options' => $vteOptions,
            '#ajax' => [
                'callback' => '::replaceMiddleTable',
                'wrapper' => 'middle-table',
                'event' => 'change',
                'method' => 'replace',
                'effect' => 'fade',
            ],
        ];

        $startYearTree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('start_year');
        $startYearOptions = ['all' => t('Alle')];
        foreach ($startYearTree as $startYearOption) {
            $startYearOptions[$startYearOption->tid] = $startYearOption->name;
        }
        $element['main_form_content']['middle']['selects']['start_year'] = [
            '#type' => 'select',
            '#title' => t('Volgens Startjaar'),
            '#options' => $startYearOptions,
            '#ajax' => [
                'callback' => '::replaceMiddleTable',
                'wrapper' => 'middle-table',
                'event' => 'change',
                'method' => 'replace',
                'effect' => 'fade',
            ],
        ];

        $budgetOptionsTree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('budget');
        $budgetOptions = ['all' => t('Alle')];
        foreach ($budgetOptionsTree as $budgetOption) {
            $budgetOptions[$budgetOption->tid] = $budgetOption->name;
        }
        $element['main_form_content']['middle']['selects']['budget'] = [
            '#type' => 'select',
            '#title' => t('Werkingsbudget'),
            '#options' => $budgetOptions,
            //      '#default_value' => $subsectorOptions,
            '#ajax' => array(
                'callback' => '::replaceMiddleTable',
                'wrapper' => 'middle-table',
                'event' => 'change',
                'method' => 'replace',
                'effect' => 'fade',
            ),
        ];

      if (!empty($sector)) {
        $categoryTotals = $this->calculationService->calculateTotalsPerCategory($sector, $vte, $start_year, $budget);
      } else {
        $categoryTotals = $this->calculationService->calculateTotalsPerCategory(NULL, $vte, $start_year, $budget);
      }

      if (!empty($subsector)) {
        $middleCategoryTotals = $this->calculationService->calculateTotalsPerCategory($subsector, $vte, $start_year, $budget);
      } elseif (!empty($sector)) {
        $middleCategoryTotals = $this->calculationService->calculateTotalsPerCategory(reset($sector)->id(), $vte, $start_year, $budget);
      } else {
        $middleCategoryTotals = $this->calculationService->calculateTotalsPerCategory(NULL, $vte, $start_year, $budget);
      }

        if (!empty($categoryTotals)) {
          $totalAverage = '0';
          foreach ($categoryTotals as $categoryTotal) {
            $totalAverage += $categoryTotal['avg'];
          }
          $totalAverage /= count($categoryTotals);
        }
        else {
          $totalAverage = 0;
        }

        $element['main_form_content']['middle']['middle_table'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => ['middle-table'],
            ],
            'avg' => [
                '#markup' => t('Gemiddelde selectieve score: ') . round($totalAverage) . '%',
            ],
        ];

        $element['main_form_content']['middle']['middle_table']['table'] = [
            '#type' => 'table',
            '#header' => [
                '',
                t('Score'),
                t('Gem.'),
                t('Med.'),
                t('Laagste'),
                t('Hoogste'),
                '',
            ],
        ];

        if (!$this->currentUser->isAnonymous() && count($options) > 1) {
            $element['main_form_content']['old_date'] = [
                '#type' => 'select',
                '#title' => t('Je score vergelijken met'),
                '#options' => $options,
                '#default_value' => $createdOld,
                '#ajax' => [
                    'callback' => '::replaceBottomTable',
                    'wrapper' => 'bottom-table',
                    'event' => 'change',
                    'method' => 'replace',
                    'effect' => 'fade',
                ],
            ];

            $element['main_form_content']['bottom'] = [
                '#type' => 'container',
                '#attributes' => [
                    'id' => ['bottom-table']
                ],
            ];


            $element['main_form_content']['bottom']['bottom_table'] = [
                '#type' => 'table',
                '#header' => [
                    '',
                    date('d M Y', $created),
                    date('d M Y', $createdOld),
                ],
            ];

            $element['main_form_content']['bottom']['bottom_diagram'] = [
                '#type' => 'container',
                '#attributes' => [
                    'id' => 'bottom-diagram'
                ],
            ];

            $element['main_form_content']['bottom']['bottom_diagram']['bottom_chart'] = [
                '#type' => 'container',
                '#attributes' => [
                    'id' => ['bottom-chart-wrapper'],
                    'style' => ['width: 200%;'],
                ],
                '#attached' => [
                    'library' => [
                        'general/chartjs.chart',
                        'general/general.chart',
                    ],
                    'drupalSettings' => [
                        'generateChartBottom' => [],
                    ],
                ],
                'html' => [
                    '#type' => 'html_tag',
                    '#tag' => 'canvas',
                    '#attributes' => [
                        'id' => ['bottom-chart'],
                    ],
                ],
            ];

            $element['main_form_content']['bottom']['bottom_diagram']['bottom_chart_legend'] = [
                'general' => [
                    '#markup' => '<div class="legend"><span>'.t('Je score').'</span><br><span>'.t('Score andere sessie').'</span></div>',
                ],
            ];
        }

        // Render PDF download link.
        $element['main_form_content']['resultaat'] = [
            '#type' => 'container',
            '#attributes' => ['id' => 'resultaat'],
            'pdf' => [
                '#markup' => '<div id="download-resultaat"><h2>'.t('Download resultaat').'</h2><a href="#" id="download-pdf"><span>' . t('Download als PDF') . '</span></a></div>',
            ],
        ];

        if (!$this->currentUser->isAnonymous()) {
            // Get the options.
            $privacyTree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('privacy_options');
            $privacyOptions = [];
            foreach ($privacyTree as $privacyOption) {
                $privacyOptions[$privacyOption->tid] = $privacyOption->name;
            }
            $questionaryId = $ids[$created];
            $questionAryEntity = Questionairy::load($questionaryId);

          $element['main_form_content']['resultaat']['privacy'] = [
            '#type' => 'container',
            '#attributes' => ['id' => 'privacy'],
          ];

            $element['main_form_content']['resultaat']['privacy']['share'] = [
                '#type' => 'radios',
                '#options' => $privacyOptions,
                '#required' => TRUE,
                'title' => [
                    '#type' => 'html_tag',
                    '#tag' => 'h2',
                    '#value' => t('Deel je score'),
                ],
                '#prefix' => '<div class="edit-privacy">',
                '#suffix' => '</div>',
                '#ajax' => [
                    'callback' => '::updatePrivacy',
                    'wrapper' => 'privacy',
                    'event' => 'change',
                    'method' => 'replace',
                    'effect' => 'fade',
                ],
              '#cache' => [
                'max-age' => 0,
              ],
            ];

            if (!empty($questionAryEntity->field_quest_privacy->getValue())) {
              $element['main_form_content']['resultaat']['privacy']['share']['#default_value'] = $questionAryEntity->field_quest_privacy->getValue()[0]['target_id'];
              $element['main_form_content']['resultaat']['share']['#value'] = $questionAryEntity->field_quest_privacy->getValue()[0]['target_id'];
            }
        }

        // Render explanation content.
        $element['main_form_content']['explanation'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => ['explanation-wrapper'],
            ],
            'title' => [
                '#type' => 'html_tag',
                '#tag' => 'h2',
                '#value' => t('Hoe je organisatie verbeteren'),
            ],
            'intro' => [
                '#type' => 'html_tag',
                '#tag' => 'p',
                '#value' => t('Hieronder krijg je inspiratie en tips aangereikt die je kunnen helpen om je digitale maturiteit te verbeteren. De categorieën waar je het laagste op scoorde, staan eerst gerangschikt. Voor uitgebreide inspiratie, tips en advies op maat, kun je PACKED vzw contacteren (zie onderaan deze pagina).'),
            ],
            'categories' => [],
        ];

        if (!empty($subsector)) {
            $questionTotals = $this->calculationService->calculateTotalsPerQuestion($subsector, $vte, $start_year, $budget);
        } elseif (!empty($sector)) {
            $questionTotals = $this->calculationService->calculateTotalsPerQuestion(reset($sector)->id(), $vte, $start_year, $budget);
        } else {
            $questionTotals = $this->calculationService->calculateTotalsPerQuestion(NULL, $vte, $start_year, $budget);
        }

        if ($this->currentUser->isAnonymous()) {
            $myAveragePerQuestion = !empty($anonymousValues['question']) ? $anonymousValues['question'] : [];
        } else {
            $myAveragePerQuestion = $this->calculationService->calculateUserAveragePerQuestion($created);
        }

        foreach ($categories as $categoryId => $category) {
            if (!empty($subsector)) {
                $median = $this->calculationService->calculateMedianByCategory($categoryId, $subsector, $vte, $start_year, $budget);
            } elseif (!empty($sector)) {
                $median = $this->calculationService->calculateMedianByCategory($categoryId, reset($sector)->id(), $vte, $start_year, $budget);
            } else {
                $median = $this->calculationService->calculateMedianByCategory($categoryId, NULL, $vte, $start_year, $budget);
            }

            $element['main_form_content']['top_table_container']['top_diagram']['top_chart']['#attached']['drupalSettings']['generateChartTop']['user'][] = !empty($myAveragePerCategory[$categoryId]['average']) ? round($myAveragePerCategory[$categoryId]['average']) : '0%';
            $element['main_form_content']['top_table_container']['top_diagram']['top_chart']['#attached']['drupalSettings']['generateChartTop']['avg'][] = isset($categoryTotals[$categoryId]) ? round($categoryTotals[$categoryId]['avg']) : 0;
            $element['main_form_content']['top_table_container']['top_diagram']['top_chart']['#attached']['drupalSettings']['generateChartTop']['median'][] = round($median);
            $element['main_form_content']['top_table_container']['top_table'][$categoryId]['category_label'] = ['#plain_text' => $category['label']];
            $element['main_form_content']['top_table_container']['top_table'][$categoryId]['score'] = ['#markup' => !empty($myAveragePerCategory[$categoryId]['average']) ? '<span class="score">' . round($myAveragePerCategory[$categoryId]['average']) . '</span>' : '<span class="score">0</span>'];
            $element['main_form_content']['top_table_container']['top_table'][$categoryId]['avg'] = ['#markup' => isset($categoryTotals[$categoryId])  ? '<span class="avg">' . round($categoryTotals[$categoryId]['avg']) . '</span>' : '<span class="avg">0</span>'];
            $element['main_form_content']['top_table_container']['top_table'][$categoryId]['median'] = ['#markup' => '<span class="median">' . round($median) . '</span>'];
            $element['main_form_content']['top_table_container']['top_table'][$categoryId]['min'] = ['#markup' => isset($categoryTotals[$categoryId]) ? '<span class="min">' . round($categoryTotals[$categoryId]['min']) . '</span>' : '<span class="min">0</span>'];
            $element['main_form_content']['top_table_container']['top_table'][$categoryId]['max'] = ['#markup' => isset($categoryTotals[$categoryId]) ? '<span class="max">' . round($categoryTotals[$categoryId]['max']) . '</span>' : '<span class="min">0</span>'];

            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['#attributes'] = ['class' => 'category'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['category_label'] = ['#plain_text' => $category['label']];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['score'] = ['#plain_text' => !empty($myAveragePerCategory[$categoryId]['average']) ? round($myAveragePerCategory[$categoryId]['average']) . '%' : '0%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['avg'] = ['#plain_text' => isset($middleCategoryTotals[$categoryId]) ? round($middleCategoryTotals[$categoryId]['avg']) . '%' : '0%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['median'] = ['#plain_text' => round($median) . '%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['min'] = ['#plain_text' => isset($middleCategoryTotals[$categoryId]) ? round($middleCategoryTotals[$categoryId]['min']) . '%' : '0%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['max'] = ['#plain_text' => isset($middleCategoryTotals[$categoryId]) ? round($middleCategoryTotals[$categoryId]['max']) . '%' : '0%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['dropdown'] = ['#plain_text' => ''];

            foreach ($category['questions'] as $questionId => $question) {
                if (!empty($subsector)) {
                    $questionMedian = $this->calculationService->calculateMedianByQuestion($questionId, $subsector, $vte, $start_year, $budget);
                } elseif (!empty($sector)) {
                    $questionMedian = $this->calculationService->calculateMedianByQuestion($questionId, reset($sector)->id(), $vte, $start_year, $budget);
                } else {
                    $questionMedian = $this->calculationService->calculateMedianByQuestion($questionId, NULL, $vte, $start_year, $budget);
                }

                $html = [
                    '#markup' => $question,
                ];
                if (!empty($questionDescriptions[$questionId])) {
                    $html = [
                        '#markup' => $question . ' <a href=#>' . t('Lees je toelichting') . '</a> <div class="description">' . $questionDescriptions[$questionId]['description'] . '</div>'
                    ];
                }
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['#attributes'] = ['class' => 'question'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['question_label'] = [$html];

                if (isset($nanPerQuestion[$questionId])) {
                  $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['score'] = ['#plain_text' => 'NVT'];
                }
                else {
                  $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['score'] = ['#plain_text' => isset($myAveragePerQuestion[$questionId]) ? $myAveragePerQuestion[$questionId]['answer_value'] . '%' : '0%'];
                }

                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['avg'] = ['#plain_text' => isset($questionTotals[$questionId]) ? round($questionTotals[$questionId]['avg']) . '%' : '0%'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['median'] = ['#plain_text' => round($questionMedian) . '%'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['min'] = ['#plain_text' => isset($questionTotals[$questionId]) ? round($questionTotals[$questionId]['min']) . '%' : '0%'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['max'] = ['#plain_text' => isset($questionTotals[$questionId]) ? round($questionTotals[$questionId]['max']) . '%' : '0%'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['dropdown'] = ['#plain_text' => ''];
            }

            if (!$this->currentUser->isAnonymous() && count($options) > 1) {
                $element['main_form_content']['bottom']['bottom_chart']['#attached']['drupalSettings']['generateChartBottom']['user'][] = isset($myAveragePerCategory[$categoryId]) ? round($myAveragePerCategory[$categoryId]['average']) : 0;
                $element['main_form_content']['bottom']['bottom_chart']['#attached']['drupalSettings']['generateChartBottom']['avg'][] = isset($myAveragePerCategoryOld[$categoryId]) ? round($myAveragePerCategoryOld[$categoryId]['average']) : 0;
                $element['main_form_content']['bottom']['bottom_table'][$categoryId]['category_label'] = ['#plain_text' => $category['label']];
                $element['main_form_content']['bottom']['bottom_table'][$categoryId]['score_1'] = ['#markup' => isset($myAveragePerCategory[$categoryId]) ? '<span>' . round($myAveragePerCategory[$categoryId]['average']) . '</span>' : '<span>0</span>'];
                $element['main_form_content']['bottom']['bottom_table'][$categoryId]['score_2'] = ['#markup' => isset($myAveragePerCategoryOld[$categoryId]) ? '<span>' . round($myAveragePerCategoryOld[$categoryId]['average']) . '</span>' : '<span>0</span>'];
            }
        }

        $arrayColumnAverage = array_diff(array_combine(array_keys($myAveragePerCategory), array_column($myAveragePerCategory, 'average')), [null]);
        asort($arrayColumnAverage);
        foreach ($arrayColumnAverage as $categoryId => $avg) {
            $element['main_form_content']['explanation']['categories'][$categoryId] = [
                '#type' => 'container',
                '#attributes' => [
                    'class' => [preg_replace('/\W+/', '', strtolower(strip_tags($categories[$categoryId]['label'])))],
                    'id' => 'category-' . $categoryId,
                ],
                'title' => [
                    '#markup' => '<div class="title"><h4>' . $categories[$categoryId]['label'] . '</h4></div>',
                ],
                'description' => [
                    '#markup' => '<div class="description">' . $categories[$categoryId]['description'] . '</div>'
                ],
            ];
        }

        return $element;
    }

  /**
   * Update privacy callback.
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
    public function updatePrivacy(&$form, FormStateInterface &$form_state) {
      $date = $form_state->getValue('start_date');

      $answerDates = $this->calculationService->calculateUserCreatedDates();
      $ids = [];
      foreach ($answerDates as $dateEach) {
        $ids[$dateEach->created] = $dateEach->id;
      }

      $questionAryEntity = Questionairy::load($ids[$date]);

      if ($form_state->getTriggeringElement()['#type'] === 'radio') {
        $privacy = $form_state->getValue('share');
        $questionAryEntity->set('field_quest_privacy', ['target_id' => $privacy]);
        $questionAryEntity->save();
      }
    }

    /**
     * Form callback when re-rendering the form on an ajax request.
     *
     * @param array $form
     *   Form render array.
     * @param FormStateInterface $form_state
     *   Form state.
     *
     * @return array
     *   Form render array to display after the ajax request.
     */
    public function replaceMain(&$form, FormStateInterface &$form_state)
    {
        $date = $form_state->getValue('start_date');
        $form_state->set('start_date', $date);

        $answerDates = $this->calculationService->calculateUserCreatedDates();

        $ids = [];
        foreach ($answerDates as $dateEach) {
            $ids[$dateEach->created] = $dateEach->id;
        }

        $questionAryEntity = Questionairy::load($ids[$date]);
        $privacy = $questionAryEntity->get('field_quest_privacy')->getValue()[0]['target_id'];
        $form['main_form_content']['top_table_container']['top_diagram']['top_chart']['#attached']['drupalSettings']['privacy'] = $privacy;

        $form_state->setRebuild();
        return $form;
    }

    /**
     * Form callback when re-rendering the form on an ajax request.
     *
     * @param array $form
     *   Form render array.
     * @param FormStateInterface $form_state
     *   Form state.
     *
     * @return array
     *   Form render array to display after the ajax request.
     */
    public function replaceMiddleTable(&$form, FormStateInterface &$form_state)
    {
        $globalValues = $form_state->get('globalValues');
        $element = [];
        $subsector = $form_state->getValue('subsector');
        $vte = $form_state->getValue('vte');
        $start_year = $form_state->getValue('start_year');
        $budget = $form_state->getValue('budget');

        $form_state->set('subsector', $subsector);
        $form_state->set('vte', $vte);
        $form_state->set('start_year', $start_year);
        $form_state->set('budget', $budget);

        if (!empty($form_state->get('start_date'))) {
            $created = $form_state->get('start_date');
        } elseif (!empty($form_state->getValue('start_date'))) {
            $created = $form_state->getValue('start_date');
        } else {
            $answerDates = $this->calculationService->calculateUserCreatedDates();

            $options = [];
            foreach ($answerDates as $date) {
                $options[$date->created] = date('d m Y', $date->created);
            }

            $created = key($options);
        }

        $questionDescriptions = $this->calculationService->getUserDescriptions($created);
        if (!empty($subsector)) {
            $categoryTotals = $this->calculationService->calculateTotalsPerCategory($subsector, $vte, $start_year, $budget);
        } elseif (!empty($sector)) {
            $categoryTotals = $this->calculationService->calculateTotalsPerCategory(reset($sector)->id(), $vte, $start_year, $budget);
        } else {
            $categoryTotals = $this->calculationService->calculateTotalsPerCategory(NULL, $vte, $start_year, $budget);
        }

        $myAveragePerCategory = $this->calculationService->calculateUserAveragePerCategory($created);

        if (!empty($subsector)) {
            $questionTotals = $this->calculationService->calculateTotalsPerQuestion($subsector, $vte, $start_year, $budget);
        } elseif (!empty($sector)) {
            $questionTotals = $this->calculationService->calculateTotalsPerQuestion(reset($sector)->id(), $vte, $start_year, $budget);
        } else {
            $questionTotals = $this->calculationService->calculateTotalsPerQuestion(NULL, $vte, $start_year, $budget);
        }

        $myAveragePerQuestion = $this->calculationService->calculateUserAveragePerQuestion($created);

        if (!empty($categoryTotals)) {
          $totalAverage = '0';
          foreach ($categoryTotals as $categoryTotal) {
            $totalAverage += $categoryTotal['avg'];
          }
          $totalAverage /= count($categoryTotals);
        }
        else {
          $totalAverage = 0;
        }

        $element['main_form_content']['middle']['middle_table'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => ['middle-table'],
            ],
            'avg' => [
                '#markup' => t('Gemiddelde selectieve score: ') . round($totalAverage) . '%',
            ],
        ];

        $element['main_form_content']['middle']['middle_table']['table'] = [
            '#type' => 'table',
            '#header' => [
                '',
                t('Score'),
                t('Gem.'),
                t('Med.'),
                t('Laagste'),
                t('Hoogste'),
                '',
            ],
        ];

        $categories = $this->getCategories();
        foreach ($categories as $categoryId => $category) {
            if (!empty($subsector)) {
                $median = $this->calculationService->calculateMedianByCategory($categoryId, $subsector, $vte, $start_year, $budget);
            } elseif (!empty($sector)) {
                $median = $this->calculationService->calculateMedianByCategory($categoryId, reset($sector)->id(), $vte, $start_year, $budget);
            } else {
                $median = $this->calculationService->calculateMedianByCategory($categoryId, NULL, $vte, $start_year, $budget);
            }

            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['#attributes'] = ['class' => 'category'];


            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['category_label'] = ['#plain_text' => $category['label']];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['score'] = ['#plain_text' => round($myAveragePerCategory[$categoryId]['average']) . '%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['avg'] = ['#plain_text' => round($categoryTotals[$categoryId]['avg']) . '%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['median'] = ['#plain_text' => round($median) . '%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['min'] = ['#plain_text' => round($categoryTotals[$categoryId]['min']) . '%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['max'] = ['#plain_text' => round($categoryTotals[$categoryId]['max']) . '%'];
            $element['main_form_content']['middle']['middle_table']['table'][$categoryId]['dropdown'] = ['#plain_text' => ''];

            foreach ($category['questions'] as $questionId => $question) {
                if (!empty($subsector)) {
                    $questionMedian = $this->calculationService->calculateMedianByQuestion($categoryId, $subsector, $vte, $start_year, $budget);
                } elseif (!empty($sector)) {
                    $questionMedian = $this->calculationService->calculateMedianByQuestion($categoryId, reset($sector)->id(), $vte, $start_year, $budget);
                } else {
                    $questionMedian = $this->calculationService->calculateMedianByQuestion($categoryId, NULL, $vte, $start_year, $budget);
                }

                $html = [
                    '#markup' => $question,
                ];
                if (!empty($questionDescriptions[$questionId])) {
                    $html = [
                        '#markup' => $question . ' <a href=#>' . t('Lees je toelichting') . '</a> <div class="description">' . $questionDescriptions[$questionId]['description'] . '</div>'
                    ];
                }

                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['#attributes'] = ['class' => 'question'];


                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['question_label'] = [$html];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['score'] = ['#plain_text' => round($myAveragePerQuestion[$questionId]['answer_value']) . '%'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['avg'] = ['#plain_text' => round($questionTotals[$questionId]['avg']) . '%'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['median'] = ['#plain_text' => round($questionMedian) . '%'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['min'] = ['#plain_text' => round($questionTotals[$questionId]['min']) . '%'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['max'] = ['#plain_text' => round($questionTotals[$questionId]['max']) . '%'];
                $element['main_form_content']['middle']['middle_table']['table']['question-' . $questionId]['dropdown'] = ['#plain_text' => ''];
            }
        }

        return $element;
    }

    /**
     * Form callback when re-rendering the form on an ajax request.
     *
     * @param array $form
     *   Form render array.
     * @param FormStateInterface $form_state
     *   Form state.
     *
     * @return array
     *   Form render array to display after the ajax request.
     */
    public function replaceBottomTable(&$form, FormStateInterface &$form_state)
    {
        if (!$startDate = $form_state->get('start_date')) {
            $startDate = $form_state->getValue('start_date');
        }
        $oldDate = $form_state->getValue('old_date');
        $form_state->set('old_date', $oldDate);

        $answerDates = $this->calculationService->calculateUserCreatedDates();

        $options = [];
        foreach ($answerDates as $date) {
            $options[$date->created] = date('d m Y', $date->created);
        }

        $element['main_form_content']['bottom'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => ['bottom-table']
            ],
        ];

        $element['main_form_content']['bottom']['bottom_table'] = [
            '#type' => 'table',
            '#header' => [
                '',
                date('d M Y', $startDate),
                date('d M Y', $oldDate),
            ],
        ];

        $myAveragePerCategory = $this->calculationService->calculateUserAveragePerCategory($startDate);
        $myAveragePerCategoryOld = $this->calculationService->calculateUserAveragePerCategory($oldDate);

        $categories = $this->getCategories();
        foreach ($categories as $categoryId => $category) {
            $element['main_form_content']['bottom']['bottom_chart']['#attached']['drupalSettings']['generateChartBottom']['user'][] = round($myAveragePerCategory[$categoryId]['average']);
            $element['main_form_content']['bottom']['bottom_chart']['#attached']['drupalSettings']['generateChartBottom']['avg'][] = round($myAveragePerCategoryOld[$categoryId]['average']);            $element['main_form_content']['bottom']['bottom_table'][$categoryId]['category_label'] = ['#plain_text' => $category['label']];
            $element['main_form_content']['bottom']['bottom_table'][$categoryId]['score_1'] = ['#markup' => '<span>' . round($myAveragePerCategory[$categoryId]['average']) . '</span>'];
            $element['main_form_content']['bottom']['bottom_table'][$categoryId]['score_2'] = ['#markup' => '<span>' . round($myAveragePerCategoryOld[$categoryId]['average']) . '</span>'];
        }

        $element['main_form_content']['bottom']['bottom_diagram'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => 'bottom-diagram'
            ],
        ];

        $element['main_form_content']['bottom']['bottom_diagram']['bottom_chart'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => ['bottom-chart-wrapper'],
                'style' => ['width: 200%;'],
            ],
            '#attached' => [
                'library' => [
                    'general/chartjs.chart',
                    'general/general.chart',
                ],
                'drupalSettings' => [
                    'generateChartBottom' => [],
                ],
            ],
            'html' => [
                '#type' => 'html_tag',
                '#tag' => 'canvas',
                '#attributes' => [
                    'id' => ['bottom-chart'],
                ],
            ],
        ];

        $element['main_form_content']['bottom']['bottom_diagram']['bottom_chart_legend'] = [
            'general' => [
                '#markup' => '<div class="legend"><span>'.t('Je score').'</span><br><span>'.t('Score andere sessie').'</span></div>',
            ],
        ];


        return $element;
    }

    /**
     * Get all categories and its linked questions.
     */
    protected function getCategories()
    {
        $categories = [];
        $categoryTerms = $this->entityTypeManager->getStorage('taxonomy_term')
            ->loadByProperties([
                'vid' => 'category',
            ]);

        foreach ($categoryTerms as $categoryTerm) {
            $categoryQuestions = $categoryTerm->field_category_questions->referencedEntities();
            $categories[$categoryTerm->id()] = [
                'label' => $categoryTerm->label(),
                'questions' => [],
                'description' => '',
            ];

            foreach ($categoryQuestions as $categoryQuestion) {
                $categories[$categoryTerm->id()]['questions'][$categoryQuestion->id()] = $categoryQuestion->label();
            }

            if (count($categoryTerm->field_category_description->getValue())) {
                $categories[$categoryTerm->id()]['description'] = $categoryTerm->field_category_description->getValue()[0]['value'];
            }
        }

        return $categories;
    }

}
