<?php

namespace Drupal\general\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Questionairy entities.
 *
 * @ingroup general
 */
class QuestionairyDeleteForm extends ContentEntityDeleteForm {


}
