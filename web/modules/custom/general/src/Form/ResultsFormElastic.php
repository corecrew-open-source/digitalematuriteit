<?php

namespace Drupal\general\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\general\ElasticsearchQueryService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the results forms.
 *
 * @TODO: For this to work we would have to normalize the items that we index so
 * that the answer entity in the index holds all the needed values like the user
 * VTE / Budget / Sector etc. This way we wouldn't have to search for
 * relationships.
 */
class ResultsFormElastic extends FormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The elasticsearch query service.
   *
   * @var \Drupal\general\ElasticsearchQueryService
   */
  protected $elasticsearchQueryService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Container keeping values which never change.
   *
   * @var array
   */
  private $globalValues;

  /**
   * Constructs a new ResultsForm.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   *   The current user.
   * @param \Drupal\general\ElasticsearchQueryService $elasticsearchQueryService
   *   The elasticsearch query service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(Connection $connection, AccountProxy $currentUser, ElasticsearchQueryService $elasticsearchQueryService, EntityTypeManagerInterface $entityTypeManager) {
    $this->connection = $connection;
    $this->elasticsearchQueryService = $elasticsearchQueryService;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $this->entityTypeManager->getStorage('user')->load($currentUser->id());

    $this->calculateGlobals();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('current_user'),
      $container->get('general.elasticsearch_query_service'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'packed_results_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->set('globalValues', $this->globalValues);

    $answerDates = $this->connection->select('answer', 'a')
      ->fields('a', ['created'])
      ->condition('user_id', $this->currentUser->id(), '=')
      ->groupBy('a.created')
      ->orderBy('a.created', 'desc')
      ->execute()
      ->fetchAll();

    $options = [];
    foreach ($answerDates as $date) {
      $options[$date->created] = date('d m Y', $date->created);
    }

    $form['start_date'] = [
      '#type' => 'select',
      '#title' => t('Results'),
      '#options' => $options,
      '#ajax' => array(
        'callback' => '::replaceMain',
        'wrapper' => 'main-form-content',
        'event' => 'change',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    ];

    $form['type_switcher']['median'] = [
      '#type' => 'html_tag',
      '#tag' => 'a',
      '#value' => t('Median'),
    ];

    $form['type_switcher']['av'] = [
      '#type' => 'html_tag',
      '#tag' => 'a',
      '#value' => t('Average'),
    ];

    $form['intro_text'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => t('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'),
    ];

    $form += $this->generateTop(key($options), $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {

  }

  /**
   * Form callback when re-rendering the form on an ajax request.
   *
   * @param array $form
   *   Form render array.
   * @param FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Form render array to display after the ajax request.
   */
  public function replaceMain(&$form, FormStateInterface &$form_state) {
    $date = $form_state->getValue('start_date');

    if (empty($this->globalValues)) {
      $this->calculateGlobals();
    }

    $this->globalValues['start_date'] = $date;
    $form_state->set('globalValues', $this->globalValues);

    return $this->generateTop($date, $form_state);
  }

  /**
   * Calculate the globals we will use multiple times.
   */
  protected function calculateGlobals() {
    $sector = $this->currentUser->field_user_sector->referencedEntities();
    // Load the user id's first for which we can show results.
    $response = $this->elasticsearchQueryService->query([
      'source' => ['uid'],
      'filters' => [
        'field_user_sector' => reset($sector)->id(),
      ],
    ]);

    $this->globalValues['uids_within_sector'] = [];
    $averages = [];
    foreach ($response->getRawResponse()['hits']['hits'] as $hit) {
      $dateResponse = $this->elasticsearchQueryService->query([
        'source' => ['created'],
        'filters' => [
          'user_id' => $hit['_source']['uid'][0],
        ],
        'aggs' => [
          'terms' => [
            'uniq_created' => 'created',
          ],
        ],
      ]);

      $this->globalValues['uids_within_sector'][$hit['_source']['uid'][0]]['created'] = [];

      foreach ($dateResponse->getRawResponse()['aggregations']['uniq_created']['buckets'] as $bucket) {
        $answerResponse = $this->elasticsearchQueryService->query([
          'filters' => [
            'user_id' => $hit['_source']['uid'][0],
            'created' => substr($bucket['key'], 0, -3),
          ],
          'aggs' => [
            'avg' => [
              'total' => 'field_ans_answer',
            ],
          ],
        ]);

        $this->globalValues['uids_within_sector'][$hit['_source']['uid'][0]]['created'][] = substr($bucket['key'], 0, -3);

        $averages[] = ceil($answerResponse->getRawResponse()['aggregations']['total']['value']);
      }
    }

    $count = count($averages);
    asort($averages);
    $mid = floor(($count - 1) / 2);
    $keys = array_slice(array_keys($averages), $mid, (1 === $count % 2 ? 1 : 2));
    $sum = 0;

    foreach ($keys as $key) {
      $sum += $averages[$key];
    }

    $median = $sum / count($keys);

    $this->globalValues['average'] = ceil(array_sum($averages) / $count);
    $this->globalValues['median'] = ceil($median);
    $this->globalValues['min'] = min($averages);
    $this->globalValues['max'] = max($averages);
  }

  /**
   * Generate the top block.
   *
   * @param string $created
   *  The created date to filter on.
   * @param FormStateInterface $form_state
   *  The form state.
   *
   * @return array
   *   Renderable form array.
   */
  protected function generateTop($created, FormStateInterface $form_state) {
    $globalValues = $form_state->get('globalValues');
    $response = $this->elasticsearchQueryService->query([
      'filters' => [
        'created' => $created,
        'user_id' => $this->currentUser->id(),
      ],
      'aggs' => [
        'avg' => [
          'total' => 'field_ans_answer',
        ],
      ],
    ]);

    $element['main_form_content'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['main-form-content']
      ],
    ];

    $element['main_form_content']['scores'] = [
      'general' => [
        '#markup' => t('Your general score') . ': ' . ceil($response->getRawResponse()['aggregations']['total']['value']) . '%',
      ],
      'average' => [
        '#markup' => t('Average score') . ': ' . $globalValues['average'] . '%',
      ],
      'median' => [
        '#markup' => t('Median score') . ': ' . $globalValues['median'] . '%',
      ],
      'minimal' => [
        '#markup' => t('Min. score') . ': ' . $globalValues['min'] . '%',
      ],
      'maximal' => [
        '#markup' => t('Max. score') . ': ' . $globalValues['max'] . '%',
      ],
    ];

    $element['main_form_content']['top_table'] = [
      '#type' => 'table',
      '#header' => [
        '',
        t('Score'),
        t('Gem.'),
        t('Med.'),
        t('Min.'),
        t('Max.'),
      ],
    ];

    $categories = $this->getCategories();
    foreach ($categories as $categoryId => $category) {
      $questionIds = array_keys($category['questions']);
      $params = [
        'filters' => [
          'created' => $created,
          'user_id' => $this->currentUser->id(),
        ],
        'aggs' => [
          'avg' => [
            'total' => 'field_ans_answer',
          ],
        ],
      ];

      $shoulds = [];
      foreach ($questionIds as $questionId) {
        $shoulds[]['match'] = ['field_ans_question' => $questionId];
      }
      $params['must'][]['bool']['should'] = $shoulds;

      $response = $this->elasticsearchQueryService->query($params);
      $userScore = ceil($response->getRawResponse()['aggregations']['total']['value']);

      $averages = [];
      foreach ($globalValues['uids_within_sector'] as $uid => $value) {
        if (!empty($value['created'])) {
          $params['filters'] = [
            'user_id' => $uid,
          ];

          $shoulds = [];
          foreach ($value['created'] as $created) {
            $shoulds[]['match'] = ['created' => $created];
          }
          $params['must'][]['bool']['should'] = $shoulds;

          $answerResponse = $this->elasticsearchQueryService->query($params);

          $averages[] = ceil($answerResponse->getRawResponse()['aggregations']['total']['value']);
        }
      }

      $count = count($averages);
      asort($averages);
      $mid = floor(($count - 1) / 2);
      $keys = array_slice(array_keys($averages), $mid, (1 === $count % 2 ? 1 : 2));
      $sum = 0;

      foreach ($keys as $key) {
        $sum += $averages[$key];
      }

      $median = $sum / count($keys);

      $avg = ceil(array_sum($averages) / $count);
      $median = ceil($median);
      $min = min($averages);
      $max = max($averages);


      $element['main_form_content']['top_table'][$categoryId]['category_label'] = ['#plain_text' => $category['label']];
      $element['main_form_content']['top_table'][$categoryId]['score'] = ['#plain_text' => $userScore];
      $element['main_form_content']['top_table'][$categoryId]['avg'] = ['#plain_text' => $avg];
      $element['main_form_content']['top_table'][$categoryId]['median'] = ['#plain_text' => $median];
      $element['main_form_content']['top_table'][$categoryId]['min'] = ['#plain_text' => $min];
      $element['main_form_content']['top_table'][$categoryId]['max'] = ['#plain_text' => $max];
    }

    return $element;
  }

  /**
   * Get all categories and its linked questions.
   */
  protected function getCategories() {
    $categories = [];
    $categoryTerms = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'category',
      ]);

    foreach ($categoryTerms as $categoryTerm) {
      $categoryQuestions = $categoryTerm->field_category_questions->referencedEntities();
      $categories[$categoryTerm->id()] = [
        'label' => $categoryTerm->label(),
        'questions' => [],
      ];

      foreach ($categoryQuestions as $categoryQuestion) {
        $categories[$categoryTerm->id()]['questions'][$categoryQuestion->id()] = $categoryQuestion->label();
      }
    }

    return $categories;
  }

}
